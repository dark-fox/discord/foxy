//Core
import { BotClient } from '../../core/BotClient';

export interface IQueueTask {
  run(Client: BotClient): IQueueTask;
  setClient(Client: BotClient): IQueueTask;
}