//Base
import { Guild, GuildChannel, GuildMember, TextChannel } from 'discord.js';

//Queue
import { QueueTask } from './QueueTask';

//Model
import { EmbedModel } from '../model/EmbedModel';
import { BirthdayModel } from '../model/BirthdayModel';
import { SettingsModel } from '../model/SettingsModel';
import { BirthdaySettingsModel } from '../model/BirthdaySettingsModel';

//Entities
import { Birthdays } from '../entities/Birthdays';
import { Settings } from '../entities/Settings';
import { Guilds } from '../entities/Guilds';
import { BirthdaySettings } from '../entities/BirthdaySettings';

export interface IBirthdayQueueGuildBirthdaySettings {
  [id: number]: BirthdaySettings;
}

export interface IBirthdayQueueGuildSettings {
  [id: number]: Settings;
}

export class BirthdayQueue extends QueueTask {
  protected expression: string = '* */12 * * *';
  protected BirthdayModel: BirthdayModel;
  protected SettingsModel: SettingsModel;
  protected BirthdaySettingsModel: BirthdaySettingsModel;
  protected Settings: IBirthdayQueueGuildBirthdaySettings = [];
  protected GuildSettings: IBirthdayQueueGuildSettings = [];

  private TodayEntries!: Birthdays[];
  private EntriesToEnd!: Birthdays[];

  constructor() {
    super();

    this.BirthdayModel = new BirthdayModel;
    this.BirthdaySettingsModel = new BirthdaySettingsModel;
    this.SettingsModel = new SettingsModel;
  }

  /**
   * Callback to run by Cron Job.
   */
  public async callback(): Promise<BirthdayQueue> {
    await this.getEntries();

    if (this.hasEntries()) {
      await this.processEntries();
    }

    return this;
  }

  /**
   * Get entries from database.
   */
  protected async getEntries(): Promise<BirthdayQueue> {
    const date: Date = new Date;
    const day: number = date.getDate();
    const month: number = date.getMonth() + 1;

    this.TodayEntries = await this.BirthdayModel.getTodayActiveEntries(day, month) || [];
    this.EntriesToEnd = await this.BirthdayModel.getEntriesToDeactivate() || [];

    return this;
  }

  /**
   * Return true if find entries to process.
   */
  protected hasEntries(): boolean {
    return 0 < this.TodayEntries.length || 0 < this.EntriesToEnd.length;
  }

  /**
   * When we've got entries, we need to iterate through them to sent wishes.
   */
  protected async processEntries(): Promise<BirthdayQueue> {
    // Celebrate users birthday.
    for (const Entry of this.TodayEntries) {
      const Guild: Guilds|undefined = Entry.guild instanceof Guilds ? Entry.guild : undefined;

      // Guild entry have to exists.
      if (Guild) {
        await this.sendWishes(Entry, Guild).catch(exception => console.error(exception));
      }
    }

    // Disable birthday celebrations
    for (const Entry of this.EntriesToEnd) {
      const Guild: Guilds|undefined = Entry.guild instanceof Guilds ? Entry.guild : undefined;

      // Guild entry have to exists.
      if (Guild) {
        await this.endBirthday(Entry, Guild).catch(exception => console.error(exception));
      }
    }

    return this;
  }

  /**
   * Send birthday wishes to each user.
   *
   * @param {Birthdays} Entry - Row from Birthdays entity.
   * @param {Guilds}    Guild - Row from Guilds entity.
   */
  protected async sendWishes(Entry: Birthdays, Guild: Guilds): Promise<this> {
    const BirthdaySettings: BirthdaySettings|undefined = await this.getGuildBirthdaySettings(Guild.id);

    // We can process only if Birthday Module is active for current Guild.
    if (BirthdaySettings && BirthdaySettings.active) {
      const DiscordGuild: Guild|undefined = this.Client.getClient().guilds.cache.get(Guild.discordId);
      const Settings: Settings|undefined = await this.getGuildSettings(Guild.id);

      if (Settings) {
        this.Reporter.setSettings(Settings);
      }

      // Check if Bot exists in Guild.
      if (DiscordGuild) {
        const User: GuildMember|undefined = DiscordGuild.members.cache.get(Entry.userId);

        // Check if User is still in the Guild.
        if (User) {
          // Send report if Report Channel is defined.
          if (Settings && Settings.reportChannel) {
            const Channel: GuildChannel|undefined = DiscordGuild.channels.cache.get(Settings.reportChannel);
            await this.Reporter.sendReport({ embed: EmbedModel.infoEmbed(`Today is <@!${Entry.userId}> birthday.`, 'Time for a Birthday Party') }, Channel);
          }

          // Send channel message if Channel is defined.
          if (BirthdaySettings.channel) {
            const Channel: TextChannel|GuildChannel|undefined = DiscordGuild.channels.cache.get(BirthdaySettings.channel);

            if (Channel && Channel instanceof TextChannel) {
              const messages: string[] = [
                `Great news! Today's <@!${Entry.userId}> birthday :gift:!`,
                `Wow! Today's <@!${Entry.userId}> birthday :gift:!`,
                `Be prepared! Today's <@!${Entry.userId}> birthday :gift:!`,
                `It's a great day! Today's <@!${Entry.userId}> birthday :gift:!`,
              ];
              const roll: number = Math.floor(Math.random() * messages.length);
              const message: string = messages[roll];

              await Channel.send(EmbedModel.infoEmbed(message, 'Birthday Party :birthday:'));
            }
          }

          let message: string = 'Today is your birthday, we wish you all the best!';

          if (BirthdaySettings.role) {
            message += ' As a gift on this special day we have a unique role for you.';

            await User.roles.add(BirthdaySettings.role);
          }

          // Replace default message with the one from database.
          if (BirthdaySettings.message) {
            message = BirthdaySettings.message;
          }

          await User.send(message).catch(exception => console.error(exception));
          await this.BirthdayModel.setEntryAsInCelebration(Entry.userId, Guild.id).catch(exception => console.error(exception));
        }
      }
    }

    return this;
  }

  /**
   * When day comes to an end, remove user role.
   *
   * @param {Birthdays} Entry - Row from Birthdays entity.
   * @param {Guilds}    Guild - Row from Guilds entity.
   */
  protected async endBirthday(Entry: Birthdays, Guild: Guilds): Promise<this> {
    const BirthdaySettings: BirthdaySettings|undefined = await this.getGuildBirthdaySettings(Guild.id);

    // We can process only if Birthday Module is active for current Guild.
    if (BirthdaySettings && BirthdaySettings.active) {
      const DiscordGuild: Guild|undefined = this.Client.getClient().guilds.cache.get(Guild.discordId);

      // Check if Bot exist in Guild.
      if (DiscordGuild) {
        const User: GuildMember|undefined = DiscordGuild.members.cache.get(Entry.userId);

        // Check if User is still in the Guild.
        if (User) {
          await User.roles.remove(BirthdaySettings.role);
          await this.BirthdayModel.turnOffEntryCelebration(Entry.userId, Guild.id).catch(exception => console.error(exception));
        }
      }
    }

    return this;
  }

  /**
   * Get birthday settings for selected Guild.
   *
   * @param {number} guildId - Id of Guild we want settings.
   */
  protected async getGuildBirthdaySettings(guildId: number): Promise<BirthdaySettings|undefined> {
    if (this.Settings.hasOwnProperty(guildId)) {
      return this.Settings[guildId];
    } else {
      const Settings: BirthdaySettings|undefined = await this.BirthdaySettingsModel.getSettings(guildId);

      if (Settings) {
        this.Settings[guildId] = Settings;
        return this.Settings[guildId];
      }
    }

    return undefined;
  }

  /**
   * Get settings for selected Guild.
   *
   * @param {number} guildId - Id of Guild we want settings.
   */
  protected async getGuildSettings(guildId: number): Promise<Settings|undefined> {
    if (this.GuildSettings.hasOwnProperty(guildId)) {
      return this.GuildSettings[guildId];
    } else {
      const Settings: Settings|undefined = await this.SettingsModel.getSettings(guildId);

      if (Settings) {
        this.GuildSettings[guildId] = Settings;
        return this.GuildSettings[guildId];
      }
    }

    return undefined;
  }

}