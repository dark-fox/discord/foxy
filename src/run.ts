//Base
import config from '../config/config.json';
import * as Discord from 'discord.js';
import { Client } from 'discord.js';

//Database
import { createConnection } from 'typeorm';
import { GuildModel } from './model/GuildModel';

//Core instance
import { Foxy } from './core/Foxy';
import { BotClient } from './core/BotClient';

let botReady: boolean = false;

const client: Client = new Discord.Client();
let FoxyInstance: Foxy;

/**
 * Connection to Discord established.
 */
client.once('ready', async () => {
  await createConnection();

  const Client: BotClient = new BotClient(client);

  FoxyInstance = new Foxy;
  await FoxyInstance.setClient(Client).then(Foxy => Foxy.run()).catch(error => console.error(error));
  botReady = await FoxyInstance.isReady();

  console.log('> discord.js client ready.');
});

/**
 * Bot joins to a Guild
 */
client.on('guildCreate', async guild => {
  if ( botReady ) {
    const Guild: GuildModel = new GuildModel;
    await Guild.storeGuild(guild);
  }
});

/**
 * Any message is sent.
 */
client.on('message', async message => {
  if ( botReady ) {
    await FoxyInstance.processMessage(message);
  }
});

client.login(config.token).then(() => {
  console.log('> discord.js client logged successfully');
});