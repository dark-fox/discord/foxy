import { Connection, createConnection } from 'typeorm';
import { SettingsCreator } from './core/SettingsCreator';

export class DbSync {
  public async sync(): Promise<void> {
    const db: Connection = await createConnection();

    return await db.synchronize();
  }
}

try {
  (new DbSync).sync()
    .then(async () => {
      console.log('> db synchronized');
      console.log('> will create settings tables');

      const Creator: SettingsCreator = new SettingsCreator;
      await Creator.createRelations().then(() => console.log('> tables created')).catch(exception => console.error(exception));
    }).catch(exception => console.error(exception)); // @ts-ignore
} catch (error) {
  console.error(error);
}
