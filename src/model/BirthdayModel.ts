//Base
import { getRepository, Repository } from 'typeorm';

//Entities
import { Birthdays, IBirthdayDate } from '../entities/Birthdays';

//Model
import { Model } from './Model';
import { IResponse } from './interfaces/IResponse';

export class BirthdayModel extends Model {
  private repository: Repository<Birthdays>;

  constructor() {
    super();
    this.repository = getRepository(Birthdays);
  }

  /**
   * Set user birthday date.
   *
   * @param {string}        userId    - User id.
   * @param {IBirthdayDate} date      - Object with day and date.
   * @param {number}        [guildId] - Id of Guild stored in database.
   */
  public async setDate(userId: string, date: IBirthdayDate, guildId: number = this.guildId): Promise<IResponse> {
    let Entry: Birthdays|undefined = await this.getUserRow(userId, guildId);
    let isNewEntry: boolean = false;
    let successMessage: string;

    if (!Entry) {
      Entry = new Birthdays;
      Entry.userId = userId;
      Entry.guild = guildId;
      isNewEntry = true;
    }

    Entry.date = date;

    if (date.day && date.month) {
      successMessage = `Birthday date was set to day: ${date.day} and month: ${date.month}.`;
    } else {
      successMessage = `Birthday feature for <@!${userId}> is now disabled.`;
    }

    return await Entry.save()
      .then(() => this.successResponse(successMessage))
      .catch(error => {
        console.info(`[INFO] User: ${userId}, isNew: ${isNewEntry ? '1' : '0'}, day: ${date.day}, month: ${date.month}, guild: ${guildId}`);
        console.error('[ERROR] BirthdayModel.setDate() error');
        console.error(error);

        return this.failResponse(`Can't set birthday date.`);
      });
  }

  /**
   * Mark user entry as celebrated.
   *
   * @param {string} userId    - User id.
   * @param {number} [guildId] - Id of Guild stored in database.
   */
  public async setEntryAsInCelebration(userId: string, guildId: number = this.guildId): Promise<IResponse> {
    let Entry: Birthdays|undefined = await this.getUserRow(userId, guildId);

    if (Entry) {
      Entry.timesCelebrated++;
      Entry.isCelebrated = true;
      Entry.timeStart = new Date;

      return await this.repository.save(Entry)
        .then(() => this.successResponse(`Entry for user: ${userId} on guild: ${guildId} mark as in celebration.`) )
        .catch(error => {
          console.info(`[INFO] User: ${userId}, guild: ${guildId}`);
          console.error('[ERROR] BirthdayModel.setEntryAsInCelebration() error');
          console.error(error);

          return this.failResponse(`Can't set entry for user ${userId} on guild ${guildId} as in celebration.`);
        });
    }

    return this.failResponse(`Can't set entry for user ${userId} on guild ${guildId} as in celebration.`);
  }

  /**
   * Mark user entry as celebrated.
   *
   * @param {string} userId    - User id.
   * @param {number} [guildId] - Id of Guild stored in database.
   */
  public async turnOffEntryCelebration(userId: string, guildId: number = this.guildId): Promise<IResponse> {
    let Entry: Birthdays|undefined = await this.getUserRow(userId, guildId);

    if (Entry) {
      Entry.isCelebrated = false;

      return await this.repository.save(Entry)
        .then(() => this.successResponse(`Entry for user: ${userId} on guild: ${guildId} mark as celebrated.`) )
        .catch(error => {
          console.info(`[INFO] User: ${userId}, guild: ${guildId}`);
          console.error('[ERROR] BirthdayModel.turnOffEntryCelebration() error');
          console.error(error);

          return this.failResponse(`Can't set entry for user ${userId} on guild ${guildId} as celebrated.`);
        });
    }

    return this.failResponse(`Can't set entry for user ${userId} on guild ${guildId} as celebrated.`);
  }

  /**
   * Get entry for user if exists.
   *
   * @param {string} userId    - User id.
   * @param {number} [guildId] - Id of Guild stored in database.
   */
  public async getUserRow(userId: string, guildId: number = this.guildId): Promise<Birthdays|undefined> {
    return await this.repository.findOne( { where: { userId, guild: guildId } });
  }

  /**
   * Get all active birthday entries.
   */
  public async getActiveEntries(): Promise<Birthdays[]|undefined> {
    return await this.repository.find( { where: { active: true }, relations: ['guild'] });
  }

  /**
   * Get all birthday entries that should be celebrated.
   *
   * @param {number} day   - Day of birth.
   * @param {number} month - Month of birth.
   */
  public async getTodayActiveEntries(day: number, month: number): Promise<Birthdays[]|undefined> {
    return await this.repository.createQueryBuilder('b')
      .leftJoinAndSelect('b.guild', 'guild')
      .where(
        'b.date @> :date AND b.active = :active AND b.is_celebrated = :celebrated',
        {
          date: { day, month },
          active: true,
          celebrated: false
        }
      )
      .getMany();
  }

  /**
   * Get all active birthday entries.
   */
  public async getEntriesToDeactivate(): Promise<Birthdays[]|undefined> {
    return await this.repository.createQueryBuilder('b')
      .leftJoinAndSelect('b.guild', 'guild')
      .where(
        'b.is_celebrated = :celebrated AND b.time_start <= NOW() - INTERVAL \'1 day\' AND b.time_start > NOW() - INTERVAL \'7 day\'',
        {
          celebrated: true,
        }
      )
      .getMany();
  }

}