import { IResponse } from './interfaces/IResponse';

export abstract class Model {
  protected guildId!: number;

  /**
   * Set value of class property guildId.
   *
   * @param {number} id - Value to set.
   */
  public setGuildId(id: number): this {
    this.guildId = id;
    return this;
  }

  /**
   * Generates IResponse compatible object with failed status.
   *
   * @param {string} response - Response content to return.
   */
  protected failResponse (response: string): IResponse {
    return { status: false, message: response };
  }

  /**
   * Generates IResponse compatible object with successful status.
   *
   * @param {string} response - Response content to return.
   */
  protected successResponse (response: string): IResponse {
    return { status: true, message: response };
  }

  /**
   * Generates IResponse compatible object.
   *
   * @param {boolean} status   - Status to return.
   * @param {string}  response - Response content to return.
   */
  protected doResponse (status: boolean, response: string): IResponse {
    return { status, message: response };
  }

}