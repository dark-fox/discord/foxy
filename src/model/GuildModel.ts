//Base
import { Guild } from 'discord.js';
import { getRepository, Repository } from 'typeorm';

//Entities
import { Guilds } from '../entities/Guilds';

//Model
import { SettingsModel } from './SettingsModel';
import { BirthdaySettingsModel } from './BirthdaySettingsModel';
import { Model } from './Model';

//Interface
import { IResponse } from './interfaces/IResponse';

export class GuildModel extends Model {
  private repository: Repository<Guilds>;

  constructor() {
    super();
    this.repository = getRepository(Guilds);
  }

  /**
   * Create entry with new Guild.
   *
   * @param {Guild} guild - Discord.js Guild instance.
   */
  public async storeGuild(guild: Guild): Promise<IResponse> {
    if (! await this.doGuildExists(guild.id)) {
      const GuildsEntity: Guilds = new Guilds;

      GuildsEntity.discordId = guild.id;
      GuildsEntity.name = guild.name;

      return await this.repository.save(GuildsEntity).then(async guild => {
        let successful = 0;
        let expected = 0;

        const settingsResult: boolean = await (new SettingsModel).createGuildEntry(guild.id)
          .then(response => {
            return !!response;
          })
          .catch(error => {
            console.error('Got error at GuildModel.storeGuild() >> SettingsModel.createGuildEntry()');
            console.error(error);

            return false;
          });

        expected++;

        if (settingsResult) { successful++; }

        const birthdaySettingsResult: boolean = await (new BirthdaySettingsModel).createGuildEntry(guild.id)
          .then(response => {
            return !!response;
          })
          .catch(error => {
            console.error('Got error at GuildModel.storeGuild() >> BirthdaySettingsModel.createGuildEntry()');
            console.error(error);

            return false;
          });

        expected++;

        if (birthdaySettingsResult) { successful++; }

        if (expected === successful) {
          return this.successResponse('All settings created successfully.');
        } else {
          return this.failResponse('Not all settings was successfully created.');
        }
      }).catch(error => {
        console.error('Got error at GuildModel.storeGuild()');
        console.error(error);

        return this.failResponse(`Guild ${guild.id} can't be stored in database.`);
      });
    }

    return this.failResponse('There was problem while bot was trying to add new entry for guild.');
  }

  /**
   * Get guild data based on provided id.
   *
   * @param {String} id - Discord Guild id.
   */
  public async getGuild(id: string): Promise<Guilds | undefined> {
    return await this.repository.findOne({discordId: id});
  }

  /**
   * Check if guild is stored in database.
   *
   * @param id
   */
  public async doGuildExists(id: string): Promise<boolean> {
    return !! await this.repository.findOne({discordId: id});
  }

  /**
   * Get all active Guilds.
   */
  public async getActiveGuilds(): Promise<Guilds[] | undefined> {
    return await this.repository.find({active: true});
  }

}