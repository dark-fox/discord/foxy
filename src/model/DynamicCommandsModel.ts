//Base
import { DeleteResult, getRepository, Repository } from 'typeorm';

//Tools
import { has } from 'lodash';

//Entities
import { DynamicCommands, DynamicCommandsTypes, IAdvancedCommand, ICommandAlias, ISimpleCommand } from '../entities/DynamicCommands';

//Model
import { Model } from './Model';
import { IResponse } from './interfaces/IResponse';

export class DynamicCommandsModel extends Model {
  private repository: Repository<DynamicCommands>;

  constructor() {
    super();
    this.repository = getRepository(DynamicCommands);
  }

  /**
   * Return list of all commands created for given Guild.
   *
   * @param {number} [guildId] - Id of Guild stored in database.
   */
  public async getGuildCommands(guildId: number = this.guildId): Promise<DynamicCommands[]> {
    return await this.repository.find({ guild: guildId });
  }

  /**
   * Store new command in database.
   *
   * @param {string}                          command   - Command name.
   * @param {DynamicCommandsTypes}            type      - Command type compatible with DynamicCommandsTypes.
   * @param {ISimpleCommand|ICommandAlias|[]} content   - Command content.
   * @param {string}                          author    - Author of command.
   * @param {number}                          [guildId] - Id of Guild stored in database.
   */
  public async createCommand(command: string, type: DynamicCommandsTypes, content: ISimpleCommand|IAdvancedCommand|ICommandAlias|never[], author: string, guildId: number = this.guildId): Promise<IResponse> {
    const DynamicCommandsEntity: DynamicCommands = new DynamicCommands;
    DynamicCommandsEntity.command = command;
    DynamicCommandsEntity.guild = guildId;
    DynamicCommandsEntity.type = type;
    DynamicCommandsEntity.content = content;
    DynamicCommandsEntity.author = author;

    return await DynamicCommandsEntity.save()
      .then(() => {
        let message: string;

        if (has(content, 'heading')) {
          message = `Command \`${command}\` successfully created.`;
        } else {
          message = `Command \`${command}\` successfully created and filed with content.`;
        }

        return this.successResponse(message);
      })
      .catch(error => {
        console.info(`[INFO] Command: ${command}; guild: ${guildId}`);
        console.error('[ERROR] DynamicCommandsModel.createCommand() error');
        console.error(error);
        console.info(content);

        return this.failResponse(`Can't add command \`${command}\` to ${guildId}.`);
      });
  }

  /**
   * Modify selected command.
   *
   * @param {string}                          command   - Command name.
   * @param {ISimpleCommand|[]} content   - Command content.
   * @param {number}                          [guildId] - Id of Guild stored in database.
   */
  public async modifyCommand(command: string, content: ISimpleCommand|never[], guildId: number = this.guildId): Promise<IResponse> {
    const Command: DynamicCommands|undefined = await this.getCommand(command, guildId);

    if (Command) {
      Command.content = content;
      return this.repository.save(Command)
        .then(() => this.successResponse(`Command \`${command}\` successfully modified.`))
        .catch(error => {
          console.error('[ERROR] DynamicCommandsModel.modifyCommand() error');
          console.error(error);

          return this.failResponse(`Can't modify command \`${command}\`.`);
        });
    } else {
      console.info(`[INFO] DynamicCommandsModel.modifyCommand() info`);
      console.info(`Attempt to modify non-existent command "${command}" for guild "${guildId}".`);

      return this.failResponse(`Command \`${command}\` can't be modified because doesn't exists. Create it first.`);
    }
  }

  /**
   * Remove selected command.
   *
   * @param {string} command   - Command name.
   * @param {number} [guildId] - Id of Guild stored in database.
   */
  public async removeCommand(command: string, guildId: number = this.guildId): Promise<IResponse> {
    const Command: DynamicCommands|undefined = await this.getCommand(command, guildId);

    if (Command) {
      return this.repository.remove(Command)
        .then(() => {
          const successMessage: string = `Command \`${command}\` successfully removed.`;

          /**
           * When we're dealing with non-alias dynamic command, we want to check and delete aliases of that command.
           */
          if (DynamicCommandsTypes.alias !== Command.type) {
            return this.removeCommandAliases(Command.id)
              .then(() => this.successResponse(successMessage))
              .catch(error => {
                console.error('[ERROR] DynamicCommandsModel.removeCommand() > removeCommandAliases() error');
                console.error(error);

                return this.failResponse(`Command \`${command}\` was deleted, but there was problem with removing aliases.`);
              });
          } else {
            return this.successResponse(successMessage);
          }
        })
        .catch(error => {
          console.error('[ERROR] DynamicCommandsModel.removeCommand() error');
          console.error(error);

          return this.failResponse(`Can't remove command \`${command}\`.`);
        });
    } else {
      console.info(`[INFO] DynamicCommandsModel.modifyCommand() info`);
      console.info(`Attempt to remove non-existent command "${command}" for guild "${guildId}".`);

      return this.failResponse(`Command \`${command}\` can't be removed because doesn't exists.`);
    }
  }

  /**
   * Return Guild's command if exists.
   *
   * @param {string} command   - Command name.
   * @param {number} [guildId] - Id of Guild stored in database.
   */
  public async getCommand(command: string, guildId: number = this.guildId): Promise<DynamicCommands|undefined> {
    return await this.repository.findOne( { where: { command, guild: guildId } });
  }

  /**
   * Return command based on it's id. Guild checking is disabled for this command.
   *
   * @param {number} commandId - Id of command to get.
   */
  public async getCommandById(commandId: number): Promise<DynamicCommands|undefined> {
    return await this.repository.findOne(commandId);
  }

  /**
   * Return possible aliases for command.
   *
   * @param {number} commandId - Id of command to check.
   * @param {number} [guildId] - Id of Guild stored in database.
   */
  public async getCommandAliases(commandId: number, guildId: number = this.guildId): Promise<DynamicCommands[]> {
    return await this.repository.find({ type: DynamicCommandsTypes.alias, id: commandId, guild: guildId })
  }

  /**
   * Remove all aliases bounded with command.
   *
   * @param {number} commandId - Id of command to check.
   * @param {number} [guildId] - Id of Guild stored in database.
   */
  public async removeCommandAliases(commandId: number, guildId: number = this.guildId): Promise<DeleteResult> {
    return await this.repository
      .createQueryBuilder()
      .delete()
      .where('guild = :guildId AND content ::jsonb @> :content', { content: { command: commandId }, guildId })
      .execute();
  }

  /**
   * Replace given command name with new one.
   *
   * @param {string} command - Command name.
   * @param {string} newName - New command name.
   * @param {number} guildId - Id of Guild stored in database.
   */
  public async renameCommand(command: string, newName: string, guildId: number = this.guildId): Promise<IResponse> {
    const Command: DynamicCommands|undefined = await this.getCommand(command, guildId);

    if (Command) {
      Command.command = newName;
      return this.repository.save(Command)
        .then(() => this.successResponse(`Command \`${command}\` successfully renamed to \`${newName}\`.`))
        .catch(error => {
          console.error('[ERROR] DynamicCommandsModel.renameCommand() error');
          console.error(error);

          return this.failResponse(`Can't rename command \`${command}\` to \`${newName}\`.`);
        });
    } else {
      console.info(`[INFO] DynamicCommandsModel.renameCommand() info`);
      console.info(`Attempt to rename non-existent command "${command}" for guild "${guildId}".`);

      return this.failResponse(`Command \`${command}\` can't be modified because doesn't exists. Create it first.`);
    }
  }

}