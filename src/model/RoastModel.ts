//Base
import { getRepository, Repository } from 'typeorm';

//Entities
import { Roast } from '../entities/Roast';

//Model
import { Model } from './Model';

export class RoastModel extends Model {
  private repository: Repository<Roast>;

  constructor() {
    super();
    this.repository = getRepository(Roast);
  }

  /**
   * Return all possible roasts.
   */
  public async getPossibleRoasts(): Promise<Roast[]> {
    return await this.repository.find({ active: true });
  }

  /**
   * Return all roasts stored in the database.
   */
  public async getRoasts(): Promise<Roast[]> {
    return await this.repository.find();
  }

}