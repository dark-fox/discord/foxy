import { MessageEmbed } from 'discord.js';

export class EmbedModel {
  static foxColor: string = '#ea7917';
  static infoColor: string = '#076fba';
  static warningColor: string = '#ddb121';
  static errorColor: string = '#ac0303';
  static successColor: string = '#44a300';

  /**
   * Create embed with blue color that means info.
   *
   * @param {string} description Embed description.
   * @param {string} title       Embed title.
   * @return {MessageEmbed}
   */
  public static infoEmbed(description: string, title: string = 'Info'): MessageEmbed {
    return EmbedModel.createEmbed(EmbedModel.infoColor, title, description);
  }

  /**
   * Create embed with yellow color that means warning.
   *
   * @param {string} description Embed description.
   * @param {string} title       Embed title.
   * @return {MessageEmbed}
   */
  public static warningEmbed(description: string, title: string = 'Warning'): MessageEmbed {
    return EmbedModel.createEmbed(EmbedModel.warningColor, title, description);
  }

  /**
   * Create embed with red color that means error.
   *
   * @param {string} description Embed description.
   * @param {string} title       Embed title.
   * @return {MessageEmbed}
   */
  public static errorEmbed(description: string, title: string = 'Something goes wrong'): MessageEmbed {
    return EmbedModel.createEmbed(EmbedModel.errorColor, title, description);
  }

  /**
   * Create embed with green color that means success.
   *
   * @param {string} description Embed description.
   * @param {string} title       Embed title.
   * @return {MessageEmbed}
   */
  public static successEmbed(description: string, title: string = 'Success'): MessageEmbed {
    return EmbedModel.createEmbed(EmbedModel.successColor, title, description);
  }

  /**
   * Wrapper for Discord.js Embed function.
   *
   * @param {string} color       Color used for Embed.
   * @param {string} title       Embed title.
   * @param {string} description Embed description
   * @param {string} [image]     Optional parameter. Will add image to Embed when set.
   * @return {MessageEmbed}
   */
  public static createEmbed(color: string, title: string, description: string, image?: string): MessageEmbed {
    const Embed: MessageEmbed = new MessageEmbed;

    Embed
      .setColor(color)
      .setTitle(title)
      .setDescription(description);

    if (image) {
      Embed.setImage(image);
    }

    return Embed;
  }

}