//Base
import { getRepository, Repository } from 'typeorm';

//Entities
import { BirthdaySettings } from '../entities/BirthdaySettings';

//Model
import { Model } from './Model';
import { IResponse } from './interfaces/IResponse';

export class BirthdaySettingsModel extends Model {
  private repository: Repository<BirthdaySettings>;

  constructor() {
    super();
    this.repository = getRepository(BirthdaySettings);
  }

  /**
   * Create new entry in GuildSettings table.
   *
   * @param {number} [guildId] - Id of guild to be stored in database.
   */
  public async createGuildEntry(guildId: number = this.guildId): Promise<IResponse> {
    if (! await this.getSettings(guildId)) {
      const SettingsEntity: BirthdaySettings = new BirthdaySettings;
      SettingsEntity.guild = guildId;

      await SettingsEntity.save()
        .then(() => this.successResponse(`Birthday Settings entry for guild ${guildId} successfully created.`))
        .catch(error => {
          console.error('[ERROR] BirthdaySettingsModel.createGuildEntry() error');
          console.error(error);

          return this.failResponse(`Can't create birthday settings entry for guild ${guildId}.`);
        });
    }

    return this.failResponse(`There was problem while bot was trying create birthday settings entry for guild ${guildId}.`);
  }

  /**
   * Toggle Birthday Module on or off.
   *
   * @param {number} [guildId] - Id of guild stored in database.
   */
  public async toggleActive(guildId: number = this.guildId): Promise<IResponse> {
    const BirthdaySettings: BirthdaySettings|undefined = await this.getSettings(guildId);

    if (BirthdaySettings) {
      BirthdaySettings.active = !BirthdaySettings.active;

      return await this.repository.save(BirthdaySettings)
        .then(() => this.successResponse(`Birthday Module now turned \`${BirthdaySettings.active ? 'on' : 'off'}\`.`))
        .catch(error => {
          console.error('[ERROR] BirthdaySettingsModel.toggleActive() error');
          console.error(error);

          return this.failResponse(`Can't set Birthday Module \`${BirthdaySettings.active ? 'on' : 'off'}\` for guild ${guildId} in database.`);
        });
    }

    return this.failResponse(`There was problem while bot was trying to toggle Birthday Module for guild ${guildId}.`);
  }

  /**
   * Set value of role that will be given to user on birthday day.
   *
   * @param {string|null} role      - Role id or null.
   * @param {number}      [guildId] - Id of guild stored in database.
   */
  public async setRole(role: string|null, guildId: number = this.guildId): Promise<IResponse> {
    const BirthdaySettings: BirthdaySettings|undefined = await this.getSettings(guildId);

    if (BirthdaySettings) {
      //@ts-ignore
      BirthdaySettings.role = role;

      return await this.repository.save(BirthdaySettings)
        .then(() => this.successResponse(`Birthday role channel set to \`${role || '--disabled--'}\`.`))
        .catch(error => {
          console.error('[ERROR] BirthdaySettingsModel.setRole() error');
          console.error(error);

          return this.failResponse(`Can't set birthday role to \`${role || '--disabled--'}\` for guild ${guildId} in database.`);
        });
    }

    return this.failResponse(`There was problem while bot was trying to set birthday role for guild ${guildId}. Role: "${role || '--disabled--'}".`);
  }

  /**
   * Set value of channel that will be given to user on birthday day.
   *
   * @param {string|null} channel   - Channel id or null.
   * @param {number}      [guildId] - Id of guild stored in database.
   */
  public async setChannel(channel: string|null, guildId: number = this.guildId): Promise<IResponse> {
    const BirthdaySettings: BirthdaySettings|undefined = await this.getSettings(guildId);

    if (BirthdaySettings) {
      //@ts-ignore
      BirthdaySettings.channel = channel;

      return await this.repository.save(BirthdaySettings)
        .then(() => this.successResponse(`Birthday channel set to \`${channel || '--disabled--'}\`.`))
        .catch(error => {
          console.error('[ERROR] BirthdaySettingsModel.setChannel() error');
          console.error(error);

          return this.failResponse(`Can't set birthday channel to \`${channel || '--disabled--'}\` for guild ${guildId} in database.`);
        });
    }

    return this.failResponse(`There was problem while bot was trying to set birthday channel for guild ${guildId}. Channel: "${channel || '--disabled--'}".`);
  }

  /**
   * Set value of message that will be send to user on birthday day.
   *
   * @param {string|null} message   - Message to set or null.
   * @param {number}      [guildId] - Id of guild stored in database.
   */
  public async setMessage(message: string|null, guildId: number = this.guildId): Promise<IResponse> {
    const BirthdaySettings: BirthdaySettings|undefined = await this.getSettings(guildId);

    if (BirthdaySettings) {
      //@ts-ignore
      BirthdaySettings.message = message;

      return await this.repository.save(BirthdaySettings)
        .then(() => this.successResponse(`Birthday message set to \`${message || '--disabled--'}\`.`))
        .catch(error => {
          console.error('[ERROR] BirthdaySettingsModel.setMessage() error');
          console.error(error);

          return this.failResponse(`Can't set birthday message to \`${message || '--disabled--'}\` for guild ${guildId} in database.`);
        });
    }

    return this.failResponse(`There was problem while bot was trying to set birthday message for guild ${guildId}. message: "${message || '--disabled--'}".`);
  }

  /**
   * Return all Settings for Guild.
   *
   * @param {number} [guildId] - Database id where Guild is stored.
   */
  public async getSettings(guildId: number = this.guildId): Promise<BirthdaySettings|undefined> {
    return await this.repository.findOne({ where: { guild: guildId } });
  }

}