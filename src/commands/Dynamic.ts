// Base
import { CollectorFilter, Message, MessageEmbed, MessageReaction, User } from 'discord.js';
import { Command } from './Command';

// Models
import { EmbedModel } from '../model/EmbedModel';
import { DynamicCommandsModel } from '../model/DynamicCommandsModel';

//Tools
import { find, get } from 'lodash';

//Entities
import { DynamicCommands, DynamicCommandsTypes, ICommandAlias, ISimpleCommand } from '../entities/DynamicCommands';

// Helpers
import { CommandProcessor } from '../helpers/CommandProcessor';
import { Validation } from '../helpers/Validation';

export class Dynamic extends Command {
  protected Commands: DynamicCommandsModel;

  constructor() {
    super();
    this.Commands = new DynamicCommandsModel;
  }

  /**
   * Main method of class. Supports 2 cases.
   * 1. When user typed `dynamic` command. This case is administration only.
   * 2. When user typed something else, check if we have command like this stored in database. If yes
   *    go further. If no - display notice about unknown command.
   */
  public async process(): Promise<this> {
    const base: string = this.getCommandName() || '';

    if ('dynamic' === base) {
      if (!await this.hasPermission()) {
        await this.sendMessage(EmbedModel.errorEmbed('You don\'t have permissions to use `dynamic` command.'));
        return this;
      }

      await this.processAdminActions();
    } else {
      await this.processDynamicCommand();
    }

    return this;
  }

  /**
   * Save Model's Guild id value.
   */
  protected setModelGuildId(): this {
    this.Commands.setGuildId(this.guildId);
    return this;
  }

  /**
   * Commands possible to use by admin.
   */
  protected async processAdminActions(): Promise<this> {
    const action: string = (this.command[1] || '').toLowerCase();

    switch (action) {
      case 'commands':
        return this.showCommands();
      case 'create':
        return this.createCommand();
      case 'change':
      case 'modify':
      case 'edit':
        return this.modifyCommand();
      case 'remove':
      case 'delete':
        return this.removeCommand();
      case 'alias':
        return this.processAlias();
      case 'rename':
        return this.renameCommand();
      default:
        return this.showDynamicCommandsHelp();
    }
  }

  /**
   * Convert alias data to correct command to display.
   *
   * @param {DynamicCommands} command - Database results.
   */
  protected async displayAlias(command: DynamicCommands): Promise<this> {
    const aliasedCommandId: number = get(command, 'content.command', 0);

    if (0 < aliasedCommandId) {
      const aliasedCommand: DynamicCommands|undefined = await this.Commands.getCommandById(aliasedCommandId);

      if (aliasedCommand) {
        switch (aliasedCommand.type) {
          case DynamicCommandsTypes.simple:
            return await this.displaySimpleCommand(aliasedCommand);
          default:
            return await this.processUnknownCommand();
        }
      } else {
        console.error('[ERROR] Dynamic.displayAlias');
        console.error(`Can't find aliased command. Alias: ${command.command}; Guild: ${command.guild}; User Guild: ${this.guildId}.`)

        return await this.processUnknownCommand();
      }
    } else {
      console.error('[ERROR] Dynamic.displayAlias');
      console.error(`Can't find aliasedCommandId. Alias: ${command.command}; Guild: ${command.guild}; User Guild: ${this.guildId}.`)

      return await this.processUnknownCommand();
    }
  }

  /**
   * When command for users exists, process it. It means - check type and run correct code.
   * At the moment we support only `simple` case. In others - display notice that command does not exits.
   */
  protected async processDynamicCommand(): Promise<this> {
    const command: DynamicCommands|undefined = await this.Commands.getCommand(this.getDynamicCommandName(0));

    if (command) {
      switch (command.type) {
        case DynamicCommandsTypes.alias:
          return await this.displayAlias(command);
        case DynamicCommandsTypes.simple:
          return await this.displaySimpleCommand(command);
        case DynamicCommandsTypes.advanced:
        default:
          return await this.processUnknownCommand();
      }
    } else {
      await this.processUnknownCommand();
    }

    return this;
  }

  /**
   * Get data stored in database and send it to channel.
   *
   * @param {DynamicCommands} command - Database results.
   */
  protected async displaySimpleCommand(command: DynamicCommands): Promise<this> {
    const heading: string = get( command, 'content.heading', '' );
    const description: string = get( command, 'content.description', '' );
    const image: string = get( command, 'content.image', '' );
    let color: string = get( command, 'content.color', '' );

    if ('' !== heading && '' !== description) {
      if ('' === color) {
        color = EmbedModel.infoColor;
      }

      const Embed: MessageEmbed = EmbedModel.createEmbed(color, heading, description, image);
      await this.sendMessage(Embed);
    } else {
      await this.processUnknownCommand()
    }

    return this;
  }

  /**
   * list of all created dynamic commands.
   */
  protected async showCommands(): Promise<this> {
    const Message: Message = await this.sendMessage('Preparing the command list...');
    const commands: DynamicCommands[] = await this.Commands.getGuildCommands(this.guildId);

    let simpleCommands: string[] = [];
    let advancedCommands: string[] = [];
    let aliases: string[] = [];

    if (0 < commands.length) {
      for (let command of commands) {
        switch (command.type) {
          case DynamicCommandsTypes.simple:
            simpleCommands.push(command.command);
            break;
          case DynamicCommandsTypes.advanced:
            advancedCommands.push(command.command);
            break;
          case DynamicCommandsTypes.alias:
            aliases.push(command.command);
            break;
        }
      }
    }

    let Embed: MessageEmbed = EmbedModel.infoEmbed('', 'Dynamic commands');

    Embed.addFields(
      { name: 'simple', value: 0 < simpleCommands.length ? simpleCommands.join(', ') : 'Simple commands not created yet.' },
      { name: 'advanced', value: 0 < advancedCommands.length ? advancedCommands.join(', ') : 'Advanced commands not created yet.' },
      { name: 'alias', value: 0 < aliases.length ? aliases.join(', ') : 'Aliases not created yet.' },
    );

    await Message.delete();
    await this.sendMessage(Embed);

    return this;
  }

  /**
   * New command creation.
   */
  protected async createCommand(): Promise<this> {
    if (await this.isCommandNameCorrect()) {
      const type: string = this.command[3] || '';

      if ('simple' !== type && 'advanced' !== type) {
        await this.sendMessage(EmbedModel.errorEmbed('You need to determine type of command: `simple` or `advanced`.'));
        return this;
      }

      const command: string = this.getDynamicCommandName();
      let commandContent: ISimpleCommand|never[] = [];

      if (4 < this.command.length && 'simple' === type) {
        commandContent = this.createSimpleCommandArguments(4);
      }

      await this.Commands.createCommand(command, DynamicCommandsTypes[type], commandContent, this.getMessageAuthor())
        .then(response => this.displayModelResponse(response))
        .catch(error => {
          console.error('[ERROR] Dynamic.createCommand() error');
          console.error(error);
        });
    }

    return this;
  }

  /**
   * Base method used for command editing. Each command type needs correct processing.
   */
  protected async modifyCommand(): Promise<this> {
    if ( await this.isCommandNameCorrect( true ) ) {
      const type: DynamicCommandsTypes = await this.getCommandType();

      switch (type) {
        case DynamicCommandsTypes.simple:
          return await this.modifySimpleCommand();
        case DynamicCommandsTypes.advanced:
          return await this.modifyAdvancedCommand();
        default:
          console.log('[ERROR] Dynamic.modifyCommand() error');
          console.log(`Unrecognized type "${type}"`);

          await this.sendMessage(EmbedModel.errorEmbed('Command was found, but it has unsupported type.', 'Unrecognizable command type'));
      }
    } else {
      await this.sendMessage(EmbedModel.errorEmbed('Command you want to modify probably does not exists.', 'Unrecognizable command'));
    }

    return this;
  }

  /**
   * Run simple command editor.
   */
  protected async modifySimpleCommand(): Promise<this> {
    const commandContent: ISimpleCommand = this.createSimpleCommandArguments(3);

    if ('' === commandContent.heading && '' === commandContent.description || 'help' === commandContent.heading) {
      await this.showDynamicSimpleCommandHelp();
      return this;
    }

    await this.Commands.modifyCommand(this.getDynamicCommandName(), commandContent)
      .then(response => this.displayModelResponse(response))
      .catch(error => {
        console.error('[ERROR] Dynamic.modifySimpleCommand() error');
        console.error(error);
      });

    return this;
  }

  /**
   * Run advanced command editor.
   */
  protected async modifyAdvancedCommand(): Promise<this> {
    await this.sendMessage(EmbedModel.infoEmbed('Feature not implemented yet. Sorry :(.'));
    return this;
  }

  /**
   * Remove command.
   */
  protected async removeCommand(): Promise<this> {
    if ( await this.isCommandNameCorrect( true ) ) {
      const commandName: string = this.getDynamicCommandName();

      await this.sendMessage(EmbedModel.errorEmbed(`Are you sure you want to remove command \`${commandName}\`?`, 'Remove command'))
        .then((message: Message) => {
          const filter: CollectorFilter = (reaction: MessageReaction, user: User) => {
            return ['👍', '👎'].includes(reaction.emoji.name) && user.id === this.Message.author.id;
          };

          message.react('👍').then(() => message.react('👎'));
          message.awaitReactions(filter, { max: 1, time: 6000, errors: ['time'] })
            .then(async (collected: MessageReaction|any) => {
              const reaction: MessageReaction|any = collected.first();

              if ('👍' === reaction.emoji.name) {
                await this.Commands.removeCommand(commandName)
                  .then(response => this.displayModelResponse(response))
                  .catch(error => {
                    console.error('[ERROR] Dynamic.removeCommand() error');
                    console.error(error);
                  });
              }

              await message.reactions.removeAll();
            })
            .catch(error => console.log(error));
        });
    } else {
      await this.sendMessage(EmbedModel.errorEmbed('Command you want to remove probably does not exists.', 'Unrecognizable command'));
    }

    return this;
  }

  /**
   * Manage alias command.
   */
  protected async processAlias(): Promise<this> {
    const type: string = this.command[2] || '';

    switch (type) {
      default:
        return this.createAlias();
      case 'remove':
        return this.showDynamicAliasCommandHelp();
    }
  }

  /**
   * Dynamic command alias creation.
   */
  protected async createAlias(): Promise<this> {
    const commandSyntax: string = 'Syntax: `[prefix]dynamic alias [original command] [alias]`';
    const originalCommand: string = this.getDynamicCommandName(2) || '';
    const alias: string = this.command[3] || '';

    if ('' === originalCommand || '' === alias) {
      await this.sendMessage(EmbedModel.errorEmbed(`Both original command and alias had to be set. ${commandSyntax}`));
    } else {
      if (originalCommand === alias) {
        await this.sendMessage(EmbedModel.errorEmbed(`You can't create alias with the same name as command. ${commandSyntax}`));
      } else {
        if (this.commandExists(alias)) {
          await this.sendMessage(EmbedModel.errorEmbed(`You can't create alias with name of static command. ${commandSyntax}`));
        } else {
          const command: DynamicCommands|undefined = await this.Commands.getCommand(originalCommand);

          if (command) {
            if (DynamicCommandsTypes.alias !== command.type) {
              const content: ICommandAlias = { command: command.id };

              await this.Commands.createCommand(alias, DynamicCommandsTypes.alias, content, this.getMessageAuthor())
                .then(response => this.displayModelResponse(response))
                .catch(error => {
                  console.error('[ERROR] Dynamic.createAlias() error');
                  console.info(`Guild: ${this.guildId}; alias: ${alias}; command: ${originalCommand}`);
                  console.error(error);
                });
            } else {
              await this.sendMessage(EmbedModel.errorEmbed(`Command \`${originalCommand}\` is already an alias. You can't create alias of alias. ${commandSyntax}`));
            }
          } else {
            await this.sendMessage(EmbedModel.errorEmbed(`Sorry, but dynamic command \`${originalCommand}\` doesn't exists. ${commandSyntax}`));
          }
        }
      }
    }

    return this;
  }

  /**
   * Rename command command name to new one.
   */
  protected async renameCommand(): Promise<this> {
    if (await this.isCommandNameCorrect(true, true) && await this.isCommandNameCorrect(false, true, 3)) {
      const command: string = this.getDynamicCommandName();
      const newName: string = this.getDynamicCommandName(3);

      if (command === newName) {
        await this.sendMessage(EmbedModel.errorEmbed('New command name is the same as original one.'));
      } else {
        if ('' === newName) {
          await this.sendMessage(EmbedModel.errorEmbed('Name of new command can\'t be empty.'));
        } else {
          await this.Commands.renameCommand(command, newName)
            .then(response => this.displayModelResponse(response))
            .catch((error) => {
              console.error('[ERROR] Dynamic.renameCommand() error');
              console.info(`Guild: ${this.guildId}; command: ${command}; newName: ${newName}`);
              console.error(error);
            });
        }
      }
    } else {
      await this.sendMessage(EmbedModel.errorEmbed('Command or alias you want to rename probably does not exists. Syntax: `[prefix]dynamic rename [original name] [new name]`', 'Unrecognizable command'));
    }

    return this;
  }

  /**
   * Verify if command name is correct and send correct messages to channel.
   *
   * @param {boolean} shouldExists - Set to true if you want to check if command already exists in database.
   * @param {boolean} hideMessage  - Set to true if you want not to send errors.
   * @param {number}  commandIndex - Index to pass to getDynamicCommandName; position of command to check in user string.
   */
  protected async isCommandNameCorrect(shouldExists: boolean = false, hideMessage: boolean = false, commandIndex: number = 2): Promise<boolean> {
    const command: string = this.getDynamicCommandName(commandIndex);

    if (this.commandExists(command)) {
      if (!hideMessage) {
        await this.sendMessage(EmbedModel.errorEmbed(`Command \`${command}\` already exists as static command. This name can't be used.`));
      }

      return false;
    }

    if ('' === command) {
      if (!hideMessage) {
        await this.sendMessage(EmbedModel.errorEmbed('Name of command can\'t be empty.'));
      }
      return false;
    }

    const commands: DynamicCommands[] = await this.Commands.getGuildCommands(this.guildId);

    if (!shouldExists) {
      if (find(commands, { command })) {
        if (!hideMessage) {
          await this.sendMessage(EmbedModel.errorEmbed(`Command \`${command}\` already exists. Use \`edit\` if you want to modify command or \`remove\` if you want to delete it.`));
        }

        return false;
      }
    } else {
      if (!find(commands, { command })) {
        if (!hideMessage) {
          await this.sendMessage(EmbedModel.errorEmbed(`Command \`${command}\` doesn't exists. You should use \`create\` before further actions.`));
        }

        return false;
      }
    }

    return true;
  }

  /**
   * Get command name in correct format. We need same format for users and for prefix in database.
   *
   * @param {number} index        - Index of created dynamic command in text written by user.
   * @param {string} [customName] - Optional parameter. If set, given name will be written in correct format.
   */
  protected getDynamicCommandName(index: number = 2, customName?: string): string {
    let command: string = customName ? customName : this.command[index] || '';
    return command.toLowerCase().trim().replace(/-/g, '_') || '';
  }

  /**
   * Provide type for current command.
   *
   * @param {number} index - Index of created dynamic command in text written by user.
   */
  protected async getCommandType(index: number = 2): Promise<DynamicCommandsTypes> {
    const command: DynamicCommands|undefined = await this.Commands.getCommand(this.getDynamicCommandName(index));

    if (command) {
      return command.type;
    }

    return -1;
  }

  /**
   * We want all of the commands to be merged into one string. Thanks to that, we'll be able to cut
   * created string here and use it in further processing.
   *
   * @param {number} startAt - Position where command arguments starts.
   */
  protected createSimpleCommandArguments(startAt: number = 3): ISimpleCommand {
    const dynamicArgs: string[] = CommandProcessor.cutString(CommandProcessor.mergeCommands(this.command, ' ', startAt) || '');
    let heading: string = dynamicArgs[0] || '';
    let description: string = dynamicArgs[1] || '';
    let image: string = '';
    let color: string = '';

    heading = heading.trim();
    description = description.trim();

    if (dynamicArgs[2]) {
      const dynamic2: string = dynamicArgs[2].trim() || '';

      if (Validation.isImageUrl(dynamic2)) {
        image = dynamic2;
      } else if (Validation.isHex(dynamic2)) {
        color = dynamic2;
      }
    }

    if (dynamicArgs[3]) {
      const dynamic3: string = dynamicArgs[3].trim() || EmbedModel.infoColor;

      if (Validation.isHex(dynamic3)) {
        color = dynamic3;
      } else if (Validation.isImageUrl(dynamic3)) {
        image = dynamic3;
      }
    }

    return { heading, description, image, color };
  }

  /**
   * Create and print Embed with help.
   */
  protected async showDynamicCommandsHelp(): Promise<this> {
    let description: string = `Command \`${this.command[1]}\` wasn't recognized as one of \`dynamic\` commands. Try use one of them:`;
    let title: string = 'Unrecognized dynamic command';

    if ( !this.command[1] ) {
      title = 'Dynamic commands help';
      description = 'Dynamic commands allows you to create command filled with your content. Result is an Embed.';
    }

    let Embed: MessageEmbed = EmbedModel.infoEmbed(description, title);

    Embed.addFields(
      { name: 'commands', value: 'Show list of created commands.' },
      { name: 'create', value: 'Create new command. Syntax: `[prefix]dynamic create [command name] [command type]`. Optionally you can pass arguments that will fill command with content. `[title | description | ?image | ?color]`.' },
      { name: 'change', value: 'Type: simple. Modify custom command. Syntax: `[prefix]dynamic modify [command name] [title | description | ?image | ?color]`. Aliases: `modify`, `edit`.' },
      { name: 'change', value: 'Type: advanced. Modifications not available yet.' },
      { name: 'remove', value: 'Remove custom command. Syntax: `[prefix]dynamic remove [command/alias]`.' },
      { name: 'alias', value: 'Create alias of given command. It doesn\'t copy command content - it\'s just reference to original. Syntax: `[prefix]dynamic alias [original command] [alias name]`.' },
      { name: 'rename', value: 'Provides possibility to rename command or alias. Syntax: `[prefix]dynamic rename [original name] [new name]`.' },
    );

    await this.sendMessage(Embed);

    return this;
  }

  /**
   * Create and print Embed with help for dynamic commands simple type.
   */
  protected async showDynamicSimpleCommandHelp(): Promise<this> {
    let Embed: MessageEmbed = EmbedModel.infoEmbed('Simple command results are displayed as Embed Content - special messages that looks like this one.', 'Simple Dynamic Command');

    Embed.addFields(
      { name: 'syntax', value: 'dynamic modify [command name] [title | description | ?image | ?color]' },
      { name: '[title]', value: 'Embed title. Must be at first position.' },
      { name: '[description]', value: 'Description, but no longer than 2048 characters. Must be at second position.' },
      { name: '[?image]', value: 'Optional parameter. URL to image with syntax like `http://some.url.gif`. Argument could be omitted and replaced by color.' },
      { name: '[?color]', value: 'Optional parameter. Embed color written as HEX notation. Default value: `#076fba`. To omit and use default one, leave empty.' },
      { name: 'example', value: '[prefix]dynamic modify [command] Hello There | I\'m general Kenobi! | https://media0.giphy.com/media/lgcUUCXgC8mEo/giphy.gif | #ddb121' },
    );

    await this.sendMessage(Embed);

    return this;
  }

  /**
   * Show help for dynamic alias command.
   */
  protected async showDynamicAliasCommandHelp(): Promise<this> {
    let Embed: MessageEmbed = EmbedModel.infoEmbed('This commands allows you to manage aliases of dynamic commands. Possible actions are listed below. Aliases are useful if you want to display same action but with different command. You\'re able to create only alias of dynamic command, not another alias or built-in command.', 'Dynamic Command Alias');

    Embed.addFields(
      { name: 'add', value: 'Create alias of existed command. Syntax: `[prefix]dynamic alias add [original command] [alias]`.' },
      { name: 'remove', value: 'Remove alias. Don\'t worry, by this way you can delete only alias. Syntax: `[prefix]dynamic alias remove [alias]`.' },
    );

    await this.sendMessage(Embed);

    return this;
  }

}