//Base
import config from '../../config/config.json';
import axios from 'axios';

//Commands
import { Command } from './Command';
import { EmbedModel } from '../model/EmbedModel';

//Tools
import { get } from 'lodash';

export class Photo extends Command {
  /**
   * Check command and run correct method.
   */
  public async process(): Promise<this> {
    const base: string = this.getCommandName();

    switch (base) {
      default:
        return this.processUnknownCommand();
      case 'fox':
        return this.processFox();
      case 'cat':
        return this.processCat();
      case 'dog':
        return this.processDog();
    }
  }

  /**
   * Save Model's Guild id value.
   */
  protected setModelGuildId(): this {
    return this;
  }

  /**
   * Run method that brings random fox picture. In this case, we're using external API.
   */
  protected async processFox(): Promise<this> {
    axios
      .get(config.rest.foxRandomPicture)
      .then(response => {
        const image: string = response.data.image || '';

        if ('' !== image) {
          const Embed = EmbedModel.createEmbed(
            EmbedModel.foxColor,
            'What a fox!',
            'Here\'s random cute fox picture, just for you!',
            image
          );

          this.sendMessage(Embed);
        }
      })
      .catch(error => {
        this.sendMessage(EmbedModel.errorEmbed('Sorry, we can\'t get random fox picture :(. Try again later.'));

        console.error(`[ERROR] Can't get random fox picture.`);
        console.error(error);
      });

    return this;
  }

  /**
   * Run method that brings random cat picture. In this case, we're using external API.
   */
  protected async processCat(): Promise<this> {
    axios
      .get(config.rest.catRandomPicture)
      .then(response => {
        const apiResponse: string[] = response.data || '';

        const image: string = get(apiResponse, '[0].url', '');

        if ('' !== image) {
          const Embed = EmbedModel.createEmbed(
            EmbedModel.infoColor,
            'What a cat!',
            'Here\'s random cute cat picture, just for you!',
            image
          );

          this.sendMessage(Embed);
        }
      })
      .catch(error => {
        this.sendMessage(EmbedModel.errorEmbed('Sorry, we can\'t get random cat picture :(. Try again later.'));

        console.error(`[ERROR] Can't get random cat picture.`);
        console.error(error);
      });

    return this;
  }

  /**
   * Run method that brings random dog picture. In this case, we're using external API.
   */
  protected async processDog(): Promise<this> {
    axios
      .get(config.rest.dogRandomPicture)
      .then(response => {
        const image: string = response.data.url || '';

        if ('' !== image) {
          const imageParts: string[] = (image.split('.'));
          const extension: string = imageParts[imageParts.length - 1].toLowerCase();

          if ('gif' === extension) {
            this.sendMessage(`Here's random cute dog picture, just for you! ${image}`);
          } else {
            const Embed = EmbedModel.createEmbed(
              EmbedModel.successColor,
              'What a dog!',
              'Here\'s random cute dog picture, just for you!',
              image
            );

            this.sendMessage(Embed);
          }
        }
      })
      .catch(error => {
        this.sendMessage(EmbedModel.errorEmbed('Sorry, we can\'t get random dog picture :(. Try again later.'));

        console.error(`[ERROR] Can't get random dog.`);
        console.error(error);
      });

    return this;
  }

}
