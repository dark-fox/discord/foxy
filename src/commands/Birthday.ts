//Core
import { Role } from 'discord.js';
import { Channel } from 'discord.js';

//Base
import { Command } from './Command';

//Tools
import { get } from 'lodash';

//Model
import { EmbedModel } from '../model/EmbedModel';
import { BirthdayModel } from '../model/BirthdayModel';
import { BirthdaySettingsModel } from '../model/BirthdaySettingsModel';
import { CommandProcessor } from '../helpers/CommandProcessor';

export class Birthday extends Command {
  protected Birthdays: BirthdayModel;
  protected BirthdaySettingsModel: BirthdaySettingsModel;

  constructor() {
    super();
    this.Birthdays = new BirthdayModel;
    this.BirthdaySettingsModel = new BirthdaySettingsModel;
  }

  /**
   * Check command and run correct method.
   */
  public async process(): Promise<this> {
    const base: string = this.getCommandName();

    switch (base) {
      default:
        return this.processUnknownCommand();
      case 'set-birthday':
        return await this.processBirthday();
      case 'toggle-birthday-module':
        return await this.processSwitch();
      case 'set-birthday-role':
        return await this.processBirthdayRole();
      case 'set-birthday-channel':
        return await this.processBirthdayChannel();
      case 'set-birthday-message':
        return await this.processBirthdayMessage();
    }
  }

  /**
   * Save Model's Guild id value.
   */
  protected setModelGuildId(): this {
    this.GuildSettings.setGuildId(this.guildId);
    this.Birthdays.setGuildId(this.guildId);
    this.BirthdaySettingsModel.setGuildId(this.guildId);

    return this;
  }

  /**
   * Toggle Birthday Module on or off.
   */
  protected async processBirthday(): Promise<this> {
    if (1 >= this.arguments.length) {
      if ('string' === typeof this.arguments[0] && 'off' === this.arguments[0]) {
        await this.Birthdays.setDate(this.getMessageAuthorId(), {})
          .then(response => {
            this.displayModelResponse(response);

            this.Birthdays.turnOffEntryCelebration(this.getMessageAuthorId())
              .catch(error => {
                console.error('[ERROR] BirthdaySettingsModel.turnOffEntryCelebration() error (setting b-day off)');
                console.error(error);
              });
          })
          .catch(error => {
            console.error('[ERROR] BirthdaySettingsModel.setDate() error (setting b-day off)');
            console.error(error);
          });
      } else {
        await this.displayHelp();
      }
    } else {
      let day: number = Number(this.arguments[0]) || 0;
      let month: number = Number(this.arguments[1]) || 0;

      if (!day || !month) {
        await this.displayHelp();
      } else {
        if (0 >= day || 31 < day) {
          await this.sendMessage(EmbedModel.errorEmbed('Day should be a number in a range of 1 - 31.'));
          return this;
        }

        if (0 >= month || 12 < month) {
          await this.sendMessage(EmbedModel.errorEmbed('Month should be a number in a range of 1 - 12.'));
          return this;
        }

        const monthsDays = {
          1: 31,
          2: 29,
          3: 31,
          4: 30,
          5: 31,
          6: 30,
          7: 31,
          8: 31,
          9: 30,
          10: 31,
          11: 30,
          12: 31,
        };

        const maxDays: number = get(monthsDays, month, 0);

        if (0 >= day || maxDays < day) {
          await this.sendMessage(EmbedModel.errorEmbed(`Month \`${month}\` has \`${maxDays}\` days. You have to select day in a range of 1 - ${maxDays}.`));
          return this;
        }

        await this.Birthdays.setDate(this.getMessageAuthorId(), { day, month })
          .then(response => {
            this.Reporter.sendReport({ content: `<@!${this.getMessageAuthorId()}> set birthday to d: ${day}, m: ${month}.`});
            this.displayModelResponse(response);
          })
          .catch(error => {
            console.error('[ERROR] BirthdaySettingsModel.setDate() error');
            console.error(error);
          });
      }
    }

    return this;
  }

  /**
   * Toggle Birthday Module on or off.
   */
  protected async processSwitch(): Promise<this> {
    if (await this.hasPermission()) {
      await this.BirthdaySettingsModel.toggleActive()
        .then(response => this.displayModelResponse(response))
        .catch(error => {
          console.error('[ERROR] BirthdaySettingsModel.toggleActive() error');
          console.error(error);
        });
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Set id of role that will be set to user on birthday day.
   */
  protected async processBirthdayRole(): Promise<this> {
    if (await this.hasPermission()) {
      if (0 >= this.arguments.length) {
        await this.displayHelp();
      } else {
        const turnOff: boolean = !!(this.arguments[0] && 'off' === this.arguments[0].toLowerCase());
        let role: string|null;

        if (turnOff) {
          role = null;
        } else {
          let roles: string[] = [];

          this.Message.mentions.roles.each((Role: Role) => {
            roles.push(Role.id);
          });

          if (0 >= roles.length) {
            await this.sendMessage('You have to mention role if you want to save it.');
            return this;
          } else {
            role = roles[0];
          }
        }

        await this.BirthdaySettingsModel.setRole(role)
          .then(response => this.displayModelResponse(response))
          .catch(error => {
            console.error('[ERROR] BirthdaySettingsModel.setRole() error');
            console.error(error);
          });
      }
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Specify id of channel where information about user birthday will be published.
   */
  protected async processBirthdayChannel(): Promise<this> {
    if (await this.hasPermission()) {
      if (0 >= this.arguments.length) {
        await this.displayHelp();
      } else {
        const turnOff: boolean = !!(this.arguments[0] && 'off' === this.arguments[0].toLowerCase());
        let channel: string|null;

        if (turnOff) {
          channel = null;
        } else {
          let channels: string[] = [];

          this.Message.mentions.channels.each((Channel: Channel) => {
            channels.push(Channel.id);
          });

          if (0 >= channels.length) {
            await this.sendMessage('You have to mention channel if you want to store it in database.');
            return this;
          } else {
            channel = channels[0];
          }
        }

        await this.BirthdaySettingsModel.setChannel(channel)
          .then(response => this.displayModelResponse(response))
          .catch(error => {
            console.error('[ERROR] BirthdaySettingsModel.setChannel() error');
            console.error(error);
          });
      }
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Specify that will be send to user.
   */
  protected async processBirthdayMessage(): Promise<this> {
    if (await this.hasPermission()) {
      if (0 >= this.arguments.length) {
        await this.displayHelp();
      } else {
        let message: string|null = CommandProcessor.mergeCommands(this.arguments, ' ');

        if (0 >= message.length) {
          message = null;
        }

        await this.BirthdaySettingsModel.setMessage(message)
          .then(response => this.displayModelResponse(response))
          .catch(error => {
            console.error('[ERROR] BirthdaySettingsModel.setMessage() error');
            console.error(error);
          });
      }
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

}
