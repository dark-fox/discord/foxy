//Base
import {
  Message,
  MessageAttachment,
  MessageEmbed,
  MessageOptions,
  Role,
  StringResolvable,
  TextChannel
} from 'discord.js';
import { get, has } from 'lodash';

//Core
import { BotClient } from '../core/BotClient';
import { Reporter } from '../core/Reporter';

//Config
import config from '../../config/config.json';
import commands from '../../config/commands.json';

//Interfaces
import { ICommand } from './interfaces/ICommand';

//Entities
import { Settings } from '../entities/Settings';

//Model
import { EmbedModel } from '../model/EmbedModel';
import { SettingsModel } from '../model/SettingsModel';
import { IResponse } from '../model/interfaces/IResponse';

//Helpers
import { Suicide } from '../helpers/Suicide';
import { VerificationResults } from '../helpers/interfaces/ISuicide';

export abstract class Command implements ICommand {
  protected arguments!: string[];
  protected Client!: BotClient;
  protected command: string[];
  protected guildId!: number;
  protected GuildSettings: SettingsModel;
  protected Message!: Message;
  protected Settings!: Settings|undefined;
  protected Suicide: Suicide;
  protected Reporter: Reporter;

  constructor(command?: string[]) {
    this.command = command || [];
    this.GuildSettings = new SettingsModel;
    this.Suicide = new Suicide;
    this.Reporter = new Reporter;
  }

  /**
   * Set database Guild GuildSettings to class property.
   */
  public async setSettings(): Promise<this> {
    this.Settings = await this.GuildSettings.getSettings(this.guildId);

    if (this.Settings) {
      this.Reporter.setSettings(this.Settings);
    }

    return this;
  }

  /**
   * Store database Guild id in class property.
   *
   * @param {number} id - Id of Guild. Should be greater than 0 if everything's ok.
   */
  public setGuildId(id: number): this {
    this.guildId = id;
    this.setModelGuildId();

    return this;
  }

  /**
   * Create command property.
   *
   * @param {string[]} command - Array with commands to store.
   */
  public setCommand(command: string[]): this {
    this.command = command;
    this.setArguments();

    return this;
  }

  /**
   * Set Discord.js Client instance.
   *
   * @param {BotClient} Client - Discord.js Client instance.
   */
  public setClient(Client: BotClient): this {
    this.Client = Client;
    return this;
  }

  /**
   * Store Message as class property.
   *
   * @param {Message} Message - Discord.js Message.
   */
  public setMessage(Message: Message): this {
    this.Message = Message;

    if (this.Message) {
      this.Reporter.setMessage(this.Message);
    }

    return this;
  }

  /**
   * Send Message to sender channel.
   *
   * @param {StringResolvable|MessageEmbed} content    - Message content.
   * @param {MessageAttachment}             attachment - Optional attachment to the message.
   */
  public async sendMessage(content: StringResolvable | MessageEmbed, attachment: MessageAttachment | undefined = undefined): Promise<Message> {
    return attachment instanceof MessageAttachment ? this.Message.channel.send(content, attachment) : this.Message.channel.send(content);
  }

  /**
   * Return boolean information if command is protected or not.
   *
   * @param {string} [command] - Command name to check.
   */
  public async isCommandDisabled(command?: string): Promise<boolean> {
    if (!command) {
      command = this.getCommandName();
    }

    return 0 <= (this.Settings?.disabledCommands || []).indexOf(command);
  }

  /**
   * Abstract method that is needed for Factory pattern. This method is run byt MessageModel.
   */
  public abstract process(): Promise<this>;

  /**
   * Method is run at the end of this.setGuildId. It could be used to set models guild id value.
   */
  protected abstract setModelGuildId(): this;

  /**
   * Do action if user wrote command that is unknown.
   */
  protected async processUnknownCommand(): Promise<this> {
    const command: string = this.getCommandName() || '';

    if ('' !== command) {
      await this.sendMessage(`Undefined command \`${this.command}\`.`);
    }

    return this;
  }

  /**
   * Force command typed by user to be in lower case.
   */
  protected getCommandName(): string {
    if (this.command[0]) {
      return this.command[0].toLowerCase();
    }

    return '';
  }

  /**
   * Remove first element from command property to create arguments property.
   */
  protected setArguments(): this {
    this.arguments = [...this.command];
    this.arguments.shift();

    return this;
  }

  /**
   * Get and display commands "help" stored in commands.json file.
   */
  protected async displayHelp(): Promise<this> {
    const help: string = get(commands, this.getCommandName() + '.help', '');
    const title: string = `Help for command "${this.getCommandName()}"`;

    await this.sendMessage(EmbedModel.infoEmbed(help, title));

    return this;
  }

  /**
   * Return boolean information if command is protected or not.
   *
   * @param {string} [command] - Command name to check.
   */
  protected isCommandProtected(command?: string): boolean {
    if (!command) {
      command = this.getCommandName();
    }

    return get(commands, command + '.protected', false);
  }

  /**
   * Send Message about no permissions.
   */
  protected async sendNoPermissionMessage(): Promise<this> {
    await this.sendMessage(EmbedModel.errorEmbed('You don\'t have permissions run this command.'));
    return this;
  }

  /**
   * Check if user is Guild Administrator.
   */
  protected isAdmin(): boolean {
    return this.Message.member?.hasPermission('ADMINISTRATOR') || false;
  }

  /**
   * Check if user role is permitted to do action.
   */
  protected async hasPermission(): Promise<boolean> {
    const roles: string[] = await this.getPermittedRoles();
    let hasPermission: boolean = false;

    if (0 < roles.length) {
      for (let role of roles) {
        const roleId: string = (role.match(/\d+/g) || []).join('');

        if (this.Message.member?.roles.cache.has(roleId) || this.Message.member?.roles.cache.some((r: Role) => r.name === role)) {
          hasPermission = true;
          break;
        }
      }
    }

    return this.isAdmin() || hasPermission;
  }

  /**
   * Get permitted roles list.
   */
  protected async getPermittedRoles(): Promise<string[]> {
    const jsonPermittedRoles: string[] = get(config, 'permissions', []);
    let dbPermittedRoles: string[] = [];

    if (this.Settings?.permittedRoles) {
      dbPermittedRoles = this.Settings?.permittedRoles;
    }

    return [...new Set(jsonPermittedRoles.concat(dbPermittedRoles))];
  }

  /**
   * Check if command exists in commands.json.
   */
  protected commandExists(command: string): boolean {
    return has(commands, command);
  }

  /**
   * Display correct Embeds for received response.
   *
   * @param {IResponse} response - Received response from Model.
   */
  protected async displayModelResponse(response: IResponse): Promise<this> {
    if (response.status) {
      await this.sendMessage(EmbedModel.successEmbed(response.message));
    } else {
      await this.sendMessage(EmbedModel.errorEmbed(response.message));
    }

    return this;
  }

  /**
   * Return formatted message author.
   */
  protected getMessageAuthor(): string {
    return `${this.Message.author.username}#${this.Message.author.discriminator}`;
  }

  /**
   * Return id of a message author.
   */
  protected getMessageAuthorId(): string {
    return this.Message.author.id;
  }

  /**
   * Verify content of user message and do actions to notify user and Staff.
   */
  protected async verifySuicideContent(): Promise<this> {
    if (this.Settings?.suicidePrevention) {
      const content: string = this.arguments.join(' ') || '';
      const verification: VerificationResults = this.Suicide.verifyContent(content.toLowerCase());

      if (0 < verification.occurrences || this.Settings.suicideAlarmLevel <= verification.dangerScore) {
        await this.sendMessage(EmbedModel.successEmbed(`Hey ${this.getMessageAuthor()}, if you need any help, just feel free to ask.`, 'Let\'s talk'));

        if (this.Settings.reportChannel) {
          try {
            // @ts-ignore
            const Embed: MessageEmbed = EmbedModel.createEmbed(
              EmbedModel.foxColor,
              'Suicide Module Warning',
              `User ${this.getMessageAuthor()} wrote:\`\`\`${this.command.join(' ')}\`\`\``
            );
            let reportContent: MessageOptions = {};

            Embed.setFooter(`Occurrences: ${verification.occurrences} | Alarm Lvl: ${verification.dangerScore}`);

            if (this.Settings.suicidePingStaff) {
              reportContent.content = '@here';
            }

            reportContent.embed = Embed;

            await this.Reporter.sendReport(reportContent);
          } catch (exception) {
            console.error('[ERROR] Command > verifySuicideContent()');
            console.error(exception);
          }
        }
      }
    }

    return this;
  }

}