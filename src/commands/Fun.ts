//Base
import { Channel, Collection, GuildChannel, GuildMember, Message, MessageAttachment, Role, Snowflake, TextChannel, User } from 'discord.js';

//Commands
import { Command } from './Command';
import { I8ballAnswer } from './interfaces/IFun';

//Model
import { EmbedModel } from '../model/EmbedModel';
import { RoastModel } from '../model/RoastModel';

//Entities
import { Roast } from '../entities/Roast';

//Helpers
import { CommandProcessor } from '../helpers/CommandProcessor';

//Tools
import { Canvas, createCanvas, loadImage } from 'canvas';

export class Fun extends Command {
  protected Roast: RoastModel;

  constructor() {
    super();
    this.Roast = new RoastModel;
  }

  /**
   * Check command and run correct method.
   */
  public async process(): Promise<this> {
    const base: string = this.getCommandName();

    switch (base) {
      default:
        return await this.processUnknownCommand();
      case '8ball':
        return await this.process8Ball();
      case 'choose':
        return await this.processChoose();
      case 'roll':
        return await this.processRoll();
      case 'wise':
        return await this.processWise();
      case 'prank':
        return await this.processPrank();
      case 'ghost-prank':
        return await this.processGhostPrank();
      case 'roast':
        return await this.processRoast();
      case 'no-roast':
        return await this.processNoRoast();
    }
  }

  /**
   * Save Model's Guild id value.
   */
  protected setModelGuildId(): this {
    this.GuildSettings.setGuildId(this.guildId);
    this.Roast.setGuildId(this.guildId);
    return this;
  }

  /**
   * Process 8ball command. It will return random positive, neutral or negative answer.
   */
  protected async process8Ball(): Promise<this> {
    const answers: I8ballAnswer[] = [
      //Positive answers
      { color: EmbedModel.successColor, message: 'Ah... yes.' },
      { color: EmbedModel.successColor, message: 'Yes, definitely.' },
      { color: EmbedModel.successColor, message: 'Yes.' },
      { color: EmbedModel.successColor, message: 'It is... yes.' },

      //Neutral answers
      { color: EmbedModel.foxColor, message: 'Can\'t predict right now.' },
      { color: EmbedModel.foxColor, message: 'Maybe yes and maybe no.' },
      { color: EmbedModel.foxColor, message: 'I don\'t know.' },
      { color: EmbedModel.foxColor, message: 'Hard to say.' },

      //Negative answers
      { color: EmbedModel.errorColor, message: 'No.' },
      { color: EmbedModel.errorColor, message: 'Nope.' },
      { color: EmbedModel.errorColor, message: 'No, I don\'t think so.' },
      { color: EmbedModel.errorColor, message: 'No, there\'s no chance.' },
    ];
    const roll: number = Math.floor(Math.random() * answers.length);
    const answer: I8ballAnswer = answers[roll];

    await this.sendMessage(EmbedModel.createEmbed(answer.color, 'The 🎱 has spoken:', answer.message));

    await this.verifySuicideContent();

    return this;
  }

  /**
   * Process choose command. This one allows bot to randomly select one of given option.
   */
  protected async processChoose(): Promise<this> {
    if (0 === this.arguments.length) {
      return this.displayHelp();
    }

    let options: string[] = this.arguments;
    const argumentsString = CommandProcessor.mergeCommands(this.arguments);

    if (argumentsString.includes('|')) {
      options = CommandProcessor.cutString(argumentsString);
    }

    const result = options[Math.floor(Math.random() * options.length)];
    await this.sendMessage(EmbedModel.infoEmbed(`\`\`\`${result}\`\`\``, 'I choose...'));

    await this.verifySuicideContent();

    return this;
  }

  /**
   * Process roll command. It generates random number in a range of 0 and 100.
   */
  protected async processRoll(): Promise<this> {
    await this.sendMessage(EmbedModel.infoEmbed(`\`\`\`${Math.floor(Math.random() * 101)}\`\`\``, 'Your number is...'));
    return this;
  }

  /**
   * Process wise command. It generates random quote by a selected user.
   */
  protected async processWise(): Promise<this> {
    let userId: string = this.getMessageAuthorId();
    let BotMessage: Message = await this.sendMessage(EmbedModel.infoEmbed('But it may take a while...', 'Time to find some wise words'));

    if (0 < this.arguments.length) {
      if (this.arguments[0].startsWith('<@') && this.arguments[0].endsWith('>')) {
        userId = this.arguments[0].slice(2, -1);

        if (userId.startsWith('!')) {
          userId = userId.slice(1);
        }
      }
    }

    let Member: GuildMember|undefined = this.Message.guild?.members.cache.get(userId);

    if (!Member) {
      Member = this.Message.guild?.members.cache.get(this.getMessageAuthorId());
      await BotMessage.delete();
      BotMessage = await this.sendMessage(EmbedModel.errorEmbed(`I can't find a good quote, so I'll check something you wrote!`, 'Ouch'));
    }

    if (Member instanceof GuildMember) {
      let channels: TextChannel[] = [];
      const messages: string[] = [];

      this.Message.guild?.channels.cache.each((Channel: Channel|TextChannel) => {
        if ('text' === Channel.type && Channel instanceof TextChannel && Channel.guild.roles.everyone.permissionsIn(Channel.id).has(['SEND_MESSAGES', 'VIEW_CHANNEL'])) {
          channels.push(Channel);
        }
      })

      for (const Channel of channels) {
        const Messages: Promise<Collection<string, Message>> = Channel.messages.fetch({ limit: 70 });

        await Messages.then((Messages: Collection<Snowflake, Message> ) => {
          const userMessages: Collection<Snowflake, Message> = Messages.filter(Message => Message.author.id === Member?.id);

          userMessages.forEach((Message: Message) => {
            let content: string = Message.content;

            if (128 >= content.length && 5 <= content.length) {
              if (!this.Settings?.prefix || (this.Settings?.prefix && (!content.startsWith(this.Settings?.prefix) && !content.startsWith(this.Settings?.prefix.toUpperCase()))) && '' !== content.trim()) {
                if (!content.startsWith('<') && !content.endsWith('>') && '!d bump' !== content) {
                  // Check if content is not only an url.
                  const testMessage: string = content.replace(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/g, '').trim();

                  if (0 < testMessage.length) {
                    content = content.replace(/\*\*/g, '').trim();

                    // Replace mentions.
                    content = content.replace(/<@[^&]\d+>|<#\d+>|<@[&]\d+>/g, (match: string) => {
                      const id: RegExpMatchArray|null = match.match(/\d+/gm);

                      if (1 === id?.length) {
                        if (match.startsWith('<@&')) {
                          const mention: Role|undefined = this.Message.guild?.roles.cache.get(id[0]);
                          return mention ? `@${mention.name}` : '[unknown role]';
                        } else if (match.startsWith('<@')) {
                          const mention: GuildMember|undefined = this.Message.guild?.members.cache.get(id[0]);
                          return mention ? `@${mention.displayName}` : '[unknown user]';
                        } else if (match.startsWith('<#')) {
                          const mention: GuildChannel|undefined = this.Message.guild?.channels.cache.get(id[0]);
                          return mention ? `#${mention.name}` : '[unknown channel]';
                        }
                      }

                      return '[unknown]';
                    });

                    // We don't want to add empty message.
                    if (0 < content.length) {
                      messages.push(content);
                    }
                  }
                }
              }
            }
          });
        });
      }

      if (0 !== messages.length) {
        const canvas: Canvas = createCanvas(1439, 621);
        const canvasContext = canvas.getContext('2d');
        const canvasBackground = await loadImage('./assets/fun/paulo.png');
        const randomMessage: string[] = messages[Math.floor(Math.random() * messages.length)].split(' ');
        const textPosition: number = 485;
        let message: string[] = [];
        let messagePosition: number = 0;
        let currentLength: number = 0;
        let lines: number = 1;
        let finalMessage: string = '';

        // Select random message.
        randomMessage.forEach(part => {
          if (!message[messagePosition]) {
            message[messagePosition] = '';
          }

          message[messagePosition] = `${message[messagePosition]} ${part}`.trim();
          currentLength += message[messagePosition].length;

          if (40 < currentLength) {
            currentLength = 0;
            messagePosition++;
            lines++;
          }
        });

        finalMessage = message.join(`\n`).trim();

        // Replace mentions with user name if possible.
        finalMessage = finalMessage.replace(/\<\@\d*\>/g, (match: string) => {
          match = match.slice(2, -1);

          const MentionedUser: GuildMember|undefined = this.Message.guild?.members.cache.get(match);

          if (MentionedUser) {
            return MentionedUser.displayName;
          }

          return '[unknown]';
        });

        // Replace emojis and animated emojis.
        finalMessage = finalMessage.replace(/\<:\w*:\d*>|\<a:\w*:\d*>/g, () => {
          return '';
        });

        // Adjust font size.
        const fitText = (canvas: Canvas, message: string) => {
          const context = canvas.getContext('2d');
          let fontSize = 64;

          do {
            context.font = `italic ${fontSize -= 10}px sans-serif`;
            fontSize -= 10;
          } while (context.measureText(finalMessage).width > canvas.width - textPosition);

          return context.font;
        };

        // Print background.
        canvasContext.drawImage(canvasBackground, 0, 0, canvas.width, canvas.height);
        canvasContext.strokeStyle = '#000000';
        canvasContext.strokeRect(0, 0, canvas.width, canvas.height);

        // Print a wise quote.
        canvasContext.font = fitText(canvas, finalMessage);
        canvasContext.fillStyle = '#ffffff';
        canvasContext.fillText(`"${finalMessage}"`, textPosition, canvas.height / (1 === lines ? 2 : lines * 1.25));

        // Print the author.
        canvasContext.font = `normal 45px sans-serif`;
        canvasContext.textAlign = 'right';
        canvasContext.fillText(`~ ${Member.displayName}`, canvas.width - 30, canvas.height - 40);

        await BotMessage.delete();
        await this.sendMessage('', new MessageAttachment(canvas.toBuffer(), 'wise-word.png'));

        return this;
      }
    }

    await BotMessage.delete();
    await this.sendMessage(EmbedModel.errorEmbed(`I'm sorry, but I can't find any messages worth to be quoted.`));
    return this;
  }

  /**
   * Prank a user.
   */
  protected async processPrank(): Promise<this> {
    if (!this.isAdmin()) {
      await this.sendMessage(EmbedModel.errorEmbed('Nope!'));
      return this;
    }

    let userId: string = '';
    let prank: string = '';

    if (0 >= this.arguments.length) {
      await this.sendMessage(EmbedModel.errorEmbed('You have to mention user to prank.'));
      return this;
    }

    if (1 >= this.arguments.length) {
      await this.sendMessage(EmbedModel.errorEmbed('You have to write a prank message.'));
      return this;
    }

    if (this.arguments[0].startsWith('<@') && this.arguments[0].endsWith('>')) {
     userId = this.arguments[0].slice(2, -1);

      if (userId.startsWith('!')) {
        userId = userId.slice(1);
      }
    } else {
      await this.sendMessage(EmbedModel.errorEmbed('First argument should be a mention.'));
      return this;
    }

    this.arguments.shift();
    prank = `<@${userId}>, ${this.arguments.join(' ')}`;

    let Member: GuildMember|undefined = this.Message.guild?.members.cache.get(userId);

    if (!Member) {
      await this.sendMessage(EmbedModel.errorEmbed(`I can't find a mentioned user to prank.`));
      return this;
    }

    let channels: TextChannel[] = [];

    // Collect channels to prank mentioned user.
    this.Message.guild?.channels.cache.each((Channel: Channel|TextChannel) => {
      // @ts-ignore
      if ('text' === Channel.type && Channel instanceof TextChannel && Channel.permissionsFor(Member)?.has('VIEW_CHANNEL', false)) {
        channels.push(Channel);
      }
    })

    for (const Channel of channels) {
      await Channel.send(prank);
    }

    await this.sendMessage(EmbedModel.successEmbed(`Done. I'm hope you're proud of yourself...`));
    return this;
  }

  /**
   * Prank a user (once more...).
   */
  protected async processGhostPrank(): Promise<this> {
    if (!this.isAdmin()) {
      await this.sendMessage(EmbedModel.errorEmbed('Nope!'));
      return this;
    }

    let userId: string = '';
    let channelId: string = '';

    if (1 >= this.arguments.length) {
      await this.sendMessage(EmbedModel.errorEmbed('You have to mention user to prank and select channel.'));
      return this;
    }

    let multiplication: number = parseInt(this.arguments[2] ?? 10);

    if (0 >= multiplication) {
      multiplication = 10;
    }

    if (this.arguments[0].startsWith('<@') && this.arguments[0].endsWith('>')) {
      userId = this.arguments[0].slice(2, -1);

      if (userId.startsWith('!')) {
        userId = userId.slice(1);
      }
    } else {
      await this.sendMessage(EmbedModel.errorEmbed('First argument should be a mention.'));
      return this;
    }

    if (this.arguments[1].startsWith('<#') && this.arguments[0].endsWith('>')) {
      channelId = this.arguments[1].slice(2, -1);

      if (channelId.startsWith('!')) {
        channelId = channelId.slice(1);
      }
    } else {
      await this.sendMessage(EmbedModel.errorEmbed('Second argument should be a channel selection.'));
      return this;
    }

    let Member: GuildMember|undefined = this.Message.guild?.members.cache.get(userId);

    if (!Member) {
      await this.sendMessage(EmbedModel.errorEmbed(`I can't find a mentioned user to prank.`));
      return this;
    }

    const Channel: GuildChannel|undefined = this.Message.guild?.channels.cache.get(channelId);

    if (Channel && Channel.isText()) {
      const message = `<@${userId}>`;

      for (let i = 0; i < multiplication; i++) {
        Channel.send(message).then(ghostMessage => ghostMessage.delete({timeout: 10}));
      }
    }

    await this.sendMessage(EmbedModel.successEmbed(`Done. I'm hope you're proud of yourself...`));
    return this;
  }

  /**
   * Do the roast.
   */
  protected async processRoast(): Promise<this> {
    const exceptions: string[] = this.Settings?.roastExceptions || [];
    const authorId: string = this.Message.author.id;
    let exceptionTriggered: boolean = false;
    let mentions: string[] = [];

    this.Message.mentions.users.each((User: User) => {
      mentions.push(User.id);
    });

    for (const exception of exceptions) {
      if (0 <= mentions.indexOf(exception)) {
        exceptionTriggered = true;
        break;
      }
    }

    const Roasts: Roast[] = await this.Roast.getPossibleRoasts();

    if (Roasts && 0 <= Roasts.length) {
      const Roast: Roast = Roasts[Math.floor(Math.random() * Roasts.length)];

      if (!exceptionTriggered) {
        await this.sendMessage(Roast.roast);
      } else {
        await this.sendMessage(`My Dear <@!${authorId}>... ${Roast.roast}`);
      }
    } else {
      console.error('[ERROR] Fun.processRoast()');
      console.error('Can\'t find any roasts.');
    }

    /**
     * If Roast author is on exception list, we want to remove him from that.
     */
    if (0 <= exceptions.indexOf(authorId)) {
      await this.processNoRoast();
    }

    return this;
  }

  /**
   * Add or remove user from no-roast list.
   */
  protected async processNoRoast(): Promise<this> {
    const userId: string = this.Message.author.id;

    await this.GuildSettings.toggleNoRoast(userId)
      .then(response => this.displayModelResponse(response))
      .catch(error => {
        console.error('[ERROR] GuildSettings.toggleNoRoast() error');
        console.error(error);
      });

    return this;
  }
}


