//Base
import { Command } from './Command';

export class Suicide extends Command {
  /**
   * Check command and run correct method.
   */
  public async process(): Promise<this> {
    const base: string = this.getCommandName();

    switch (base) {
      default:
        return this.processUnknownCommand();
      case 'suicide-module-switch':
        return await this.processSwitch();
      case 'suicide-ping-staff':
        return await this.processPingStaff();
      case 'suicide-alarm-level':
        return await this.processAlarmLevel();
    }
  }

  /**
   * Save Model's Guild id value.
   */
  protected setModelGuildId(): this {
    this.GuildSettings.setGuildId(this.guildId);
    return this;
  }

  /**
   * Toggle Suicide Prevention Module on or off.
   */
  protected async processSwitch(): Promise<this> {
    if (this.isAdmin()) {
      await this.GuildSettings.toggleSuicideModule()
        .then(response => this.displayModelResponse(response))
        .catch(error => {
          console.error('[ERROR] GuildSettings.toggleSuicideModule() error');
          console.error(error);
        });
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Toggle Suicide Prevention Module Staff pinging on reports on or off.
   */
  protected async processPingStaff(): Promise<this> {
    if (this.isAdmin()) {
      await this.GuildSettings.togglePingStaff()
        .then(response => this.displayModelResponse(response))
        .catch(error => {
          console.error('[ERROR] GuildSettings.togglePingStaff() error');
          console.error(error);
        });
    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

  /**
   * Set alarm level of Suicide Prevention Module to new value.
   */
  protected async processAlarmLevel(): Promise<this> {
    if (this.isAdmin()) {
      if (0 >= this.arguments.length) {
        await this.displayHelp();
      } else {
        let newLevel: string|number = this.arguments[0] || '';

        if (1 >= newLevel.length) {
          newLevel = '3';
        }

        newLevel = parseInt(newLevel);

        if (0 >= newLevel) {
          newLevel = 3;
        }

        await this.GuildSettings.setSuicideAlarmLevel(newLevel)
          .then(response => this.displayModelResponse(response))
          .catch(error => {
            console.error('[ERROR] GuildSettings.toggleSuicideModule() error');
            console.error(error);
          });
      }

    } else {
      await this.sendNoPermissionMessage();
    }

    return this;
  }

}

