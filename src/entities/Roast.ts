import { BaseEntity, Column, CreateDateColumn, Entity, Index, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity()
@Index([ 'active' ])
@Index([ 'modified' ])
@Index([ 'created' ])
export class Roast extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: "text", nullable: false })
  roast!: string;

  @Column({ nullable: false, default: true })
  active!: boolean;

  @UpdateDateColumn()
  modified!: Date;

  @CreateDateColumn()
  created!: Date;

}