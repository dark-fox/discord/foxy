import {
  BaseEntity,
  Column, CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import { Guilds } from './Guilds';

export enum DynamicCommandsTypes {
  simple,
  advanced,
  alias,
}

export interface ISimpleCommand {
  heading: string;
  description: string;
  color: string;
  image: string;
}

export interface IAdvancedCommand {}

export interface ICommandAlias {
  command: number;
}

@Entity()
@Index([ 'type' ])
@Index([ 'modified' ])
@Index([ 'created' ])
@Index([ 'guild', 'command' ], { unique: true })
export class DynamicCommands extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToOne(type => Guilds)
  @JoinColumn()
  guild!: number;

  @Column({ nullable: false })
  command!: string;

  @Column({ nullable: false })
  author!: string;

  @Column('enum', { enum: DynamicCommandsTypes, nullable: false })
  type!: DynamicCommandsTypes;

  @Column({ type: 'jsonb', nullable: false, default: [] })
  content!: ISimpleCommand|IAdvancedCommand|ICommandAlias|never[];

  @UpdateDateColumn()
  modified!: Date;

  @CreateDateColumn()
  created!: Date;

}