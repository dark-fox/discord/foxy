import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import { Guilds } from './Guilds';

export interface IBirthdayDate {
  day?: number,
  month?: number,
}

@Entity()
@Index([ 'active' ])
@Index([ 'isCelebrated' ])
@Index([ 'date' ])
@Index([ 'created' ])
@Index([ 'timeStart' ])
@Index([ 'guild', 'userId' ], { unique: true })
export class Birthdays extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToOne(type => Guilds, guild => guild.id)
  @JoinColumn()
  guild!: number|Guilds;

  @Column({ name: 'user_id', nullable: false, default: '' })
  userId!: string;

  @Column({ type: 'jsonb', nullable: false, default: [] })
  date!: IBirthdayDate;

  @Column('boolean', { default: true })
  active!: boolean;

  @Column('boolean', { name: 'is_celebrated', default: false })
  isCelebrated!: boolean;

  @Column({ name: 'times_celebrated', default: 0 })
  timesCelebrated!: number;

  @Column({ name: 'time_start', type: 'timestamp', nullable: false, default: () => 'now()' })
  timeStart!: Date;

  @UpdateDateColumn()
  modified!: Date;

  @CreateDateColumn()
  created!: Date;

}