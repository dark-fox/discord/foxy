import { BaseEntity, Index, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { Guilds } from './Guilds';

@Entity()
@Index(['active'])
@Unique(['guild'])
export class BirthdaySettings extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @OneToOne(type => Guilds)
  @JoinColumn()
  guild!: number;

  @Column('boolean', { default: true })
  active!: boolean;

  @Column({ nullable: true, default: null })
  role!: string;

  @Column({ nullable: true, default: null })
  channel!: string;

  @Column({ type: 'text', nullable: true, default: null })
  message!: string;

}