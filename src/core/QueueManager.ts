//Core
import { BotClient } from './BotClient';

//Queue
import { BirthdayQueue } from '../queue/BirthdayQueue';

export class QueueManager {
  protected Client!: BotClient;

  public run(): this {
    (new BirthdayQueue()).run(this.Client);

    return this;
  }

  public setClient(Client: BotClient): QueueManager {
    this.Client = Client;
    return this;
  }

}