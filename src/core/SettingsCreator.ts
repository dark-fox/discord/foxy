//Entity
import { Guilds } from '../entities/Guilds';

//Model
import { GuildModel } from '../model/GuildModel';
import { BirthdaySettingsModel } from '../model/BirthdaySettingsModel';

export class SettingsCreator {
  protected GuildModel: GuildModel;
  protected BirthdaySettingsModel: BirthdaySettingsModel;

  private guilds: Guilds[]|undefined = [];
  private successful: number = 0;
  private expected: number = 0;

  constructor() {
    this.GuildModel = new GuildModel;
    this.BirthdaySettingsModel = new BirthdaySettingsModel;
  }

  /**
   * Main method used to create related tables..
   */
  public async createRelations(): Promise<boolean> {
    await this.getGuilds();

    if (this.guilds) {
      for (const Guild of this.guilds) {
        await this.createBirthdayModule(Guild.id);
      }
    }

    return this.successful === this.expected;
  }

  /**
   * Get and store all Guilds from database.
   */
  protected async getGuilds(): Promise<void> {
    this.guilds = await this.GuildModel.getActiveGuilds();

    if (!this.guilds) {
      this.guilds = [];
    }
  }

  /**
   * Create Birthday Module table.
   *
   * @param {number} id - Guild id to store.
   */
  protected async createBirthdayModule(id: number): Promise<boolean> {
    this.expected++;

    return await this.BirthdaySettingsModel.createGuildEntry(id)
      .then(() => { this.successful++; return true; })
      .catch(exception => { console.error(exception); return false; });
  }

}