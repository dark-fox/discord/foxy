//Base
import { Message } from 'discord.js';
import { get } from 'lodash';

//Core
import { Prefix } from './Prefix';
import { BotClient } from './BotClient';

//Configs
import commands from '../../config/commands.json';

//Entities
import { Guilds } from '../entities/Guilds';
import { Settings } from '../entities/Settings';

//Models
import { EmbedModel } from '../model/EmbedModel';
import { GuildModel } from '../model/GuildModel';
import { SettingsModel } from '../model/SettingsModel';

//Commands
import { ICommand } from '../commands/interfaces/ICommand';
import { Dynamic } from '../commands/Dynamic';
import { Fun } from '../commands/Fun';
import { Info } from '../commands/Info';
import { Settings as SettingsCommands } from '../commands/Settings';
import { Photo } from '../commands/Photo';
import { Suicide } from '../commands/Suicide';
import { Birthday } from '../commands/Birthday';

export class MessageProcessor {
  protected commandArguments!: string[];
  protected Guild: GuildModel;
  protected GuildSettings!: Settings|undefined;
  protected Client!: BotClient;
  protected Command!: ICommand;
  protected Message!: Message;
  protected Prefix: Prefix;
  protected Settings: SettingsModel;

  private guildId: number = -1;
  private prefix!: string;

  constructor() {
    this.Guild = new GuildModel;
    this.Prefix = new Prefix;
    this.Settings = new SettingsModel;
  }

  /**
   * Set Discord.js Message class property.
   *
   * @param {Message} Message - Discord.js Message instance.
   */
  public setMessage(Message: Message): this {
    this.Message = Message;
    return this;
  }

  /**
   * Set Discord.js Client instance.
   *
   * @param {BotClient} Client - Discord.js Client instance.
   */
  public setClient(Client: BotClient): this {
    this.Client = Client;
    return this;
  }

  /**
   * Process received Message.
   */
  public async process(): Promise<this> {
    if (await this.canBeProcessed()) {
      await this.Prefix.setData(this.GuildSettings);

      if (this.isMessagePrefixCorrect()) {
        await this.setCommandArguments().setMessageFactory();

        if (this.Command) {
          await this.Command.setGuildId(this.guildId).setSettings().then(async () => {
            this.Command.setCommand(this.commandArguments).setClient(this.Client).setMessage(this.Message);
            if (! await this.Command.isCommandDisabled(this.getCommand())) {
              await this.Command.process();
            } else {
              await this.Message.channel.send(EmbedModel.warningEmbed('Sorry, but this command can\'t be used right now.', 'Can\'t use that command'));
            }
          });
        } else {
          await this.Message.channel.send(EmbedModel.warningEmbed('Sorry, but this command can\'t be used right now', 'Can\'t use that command'));
        }
      }
    }

    return this;
  }

  /**
   * Get command used by user.
   */
  protected getCommand(): string {
    if (this.commandArguments.length > 0) {
      if ('' === this.commandArguments[0]) {
        this.commandArguments.shift();
      }

      return this.commandArguments[0].toLowerCase();
    }

    return '';
  }

  /**
   * Create commands array. But we don't want to have prefix.
   */
  protected setCommandArguments(): this {
    this.commandArguments = this.Message.content.slice(this.prefix.length).split(/ +/);
    return this;
  }

  /**
   * Set correct class instance to this.Command variable.
   */
  protected async setMessageFactory(): Promise<this> {
    const type: string = this.getCommandType(this.getCommand());

    switch (type) {
      case 'dynamic':
      default:
        this.Command = new Dynamic;
        break;
      case 'settings':
        this.Command = new SettingsCommands;
        break;
      case 'fun':
        this.Command = new Fun;
        break;
      case 'photo':
        this.Command = new Photo;
        break;
      case 'info':
        this.Command = new Info;
        break;
      case 'suicide':
        this.Command = new Suicide;
        break;
      case 'birthday':
        this.Command = new Birthday;
        break;
    }

    return this;
  }

  /**
   * Get command type from JSON file.
   *
   * @param {string} command Command name.
   */
  protected getCommandType(command: string): string {
    return get(commands, command + '.type', '');
  }

  /**
   * Set Guild id based on data received in Message.
   */
  protected async setGuildId(): Promise<number> {
    const messageGuildId: string = this.Message.guild?.id || '';

    if (await this.Guild.doGuildExists(messageGuildId)) {
      const Guild: Guilds | undefined = await this.Guild.getGuild(messageGuildId);

      if (Guild) {
        this.guildId = Guild.id;
      } else {
        console.error(`[ERROR] Can't find guild "${messageGuildId}" in database.`);
      }
    }

    return this.guildId;
  }

  /**
   * Check if we cam process received Message.
   */
  private async canBeProcessed(): Promise<boolean> {
    /**
     * We don't want to process written by BotClient itself.
     */
    if (this.Message.author.bot) {
      return false;
    }

    await this.setGuildId();

    if (0 < this.guildId) {
      this.GuildSettings = await this.Settings.getSettings(this.guildId);
      return true;
    } else {
      /**
       * When we don't find guild stored in database, we can't go further. To preserve database connection,
       * the only possibility to save guild id in database is to invite bot again.
       */
        await this.Message.channel.send(EmbedModel.errorEmbed('Sorry but this server is unrecognizable. Try remove kick and invite bot again.'));
        console.error(`[ERROR] Unrecognizable server "${this.Message.guild?.id || ''}".`);
    }

    return false;
  }

  /**
   * Check if prefix used by user is correct. Additionally, we want to store used prefix as class property.
   */
  private isMessagePrefixCorrect(): boolean {
    for (let prefix of this.Prefix.getPrefixes()) {
      if (this.Message.content.startsWith(prefix)) {
        this.prefix = prefix;
        return true;
      }
    }

    return false;
  }

}