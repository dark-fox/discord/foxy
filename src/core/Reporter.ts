//Core
import { GuildChannel, Message, MessageOptions, TextChannel } from 'discord.js';

//Entities
import { Settings } from '../entities/Settings';

export class Reporter {
  protected Message!: Message;
  protected Settings!: Settings;

  /**
   * Set current Message to class property.
   *
   * @param {Message} Message - Instance of Discord.js message.
   */
  public setMessage(Message: Message): this {
    this.Message = Message;
    return this;
  }

  /**
   * Set Guild Settings to class property.
   *
   * @param {Settings} Settings - Instance of Settings Entity.
   */
  public setSettings(Settings: Settings): this {
    this.Settings = Settings;
    return this;
  }

  /**
   * Set report to specific channel if exists.
   *
   * @param {MessageOptions}                   options   - Message options to set. Bot supports content and embed only.
   * @param {TextChannel|GuildChannel|boolean} [Channel] - Channel to send report.
   */
  public async sendReport(options: MessageOptions, Channel: TextChannel|GuildChannel|boolean = false): Promise<this> {
    if (!Channel) {
      Channel = await this.getReportChannel();
    }

    if (Channel && Channel instanceof TextChannel) {
      let messageContent: MessageOptions = {};

      if (options.content && 0 <= options.content.length) {
        messageContent.content = options.content;
      }

      if (options.embed) {
        messageContent.embed = options.embed;
      }

      if (messageContent.content || messageContent.embed) {
        await Channel.send(messageContent);
      }
    }

    return this;
  }

  /**
   * Return Reporter status.
   */
  public isReady(): boolean {
    return !!(this.Message && this.Settings);
  }

  /**
   * Return TextChannel object if Bot is able to find Report Channel. In other cases - return false.
   */
  public async getReportChannel(): Promise<TextChannel|boolean> {
    if (this.isReady()) {
      try {
        // @ts-ignore
        const Channel: TextChannel = await this.Message.guild?.channels.cache.get(this.Settings.reportChannel);

        if (Channel) {
          return Channel;
        }
      } catch (exception) {
        //don't display any exception here
      }
    }

    return false;
  }

}