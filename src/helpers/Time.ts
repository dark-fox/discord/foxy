export class Time {
  /**
   * Convert given time in seconds to days, hours, minutes and seconds.
   *
   * @param {number} time Time in seconds.
   */
  public static convertSecondsToTime(time: number): string {
    const days: number = Math.floor(time / (3600*24));
    const hours: number = Math.floor(time % (3600*24) / 3600);
    const minutes: number = Math.floor(time % 3600 / 60);
    const seconds: number = Math.floor(time % 60);

    return `${days} d ${hours} h ${minutes} m ${seconds} s`;
  }

}