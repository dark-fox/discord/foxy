export interface SuspiciousWords {
  [word: string]: number
}

export interface VerificationResults {
  occurrences: number;
  dangerScore: number;
}