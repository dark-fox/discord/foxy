//Tools
import { get } from 'lodash';

//Interfaces
import { SuspiciousWords, VerificationResults } from './interfaces/ISuicide';

export class Suicide {
  protected dictionary: string[] = [
    'suicide',
    'kill myself',
    'kill me',
    'want to die',
    'want die',
    'want death',
    'commit suicide',
    'someone to kill me',
    'end my life',
    'life comes to an end',
    'life ends',
  ];

  protected wordList: SuspiciousWords = {
    to: 0.1,
    want: 0.1,
    life: 0.1,
    me: 0.2,
    myself: 0.2,
    end: 0.2,
    commit: 0.2,
    kill: 1,
    die: 1,
    death: 1,
    suicide: 1.5,
  }

  /**
   * Return class dictionary.
   */
  public getDictionary(): string[] {
    return this.dictionary;
  }

  /**
   * Verify given string if contains words from the lists.
   *
   * @param {string} content - String to verify.
   */
  public verifyContent(content: string): VerificationResults  {
    return {
      occurrences: this.dictionaryOccurrences(content),
      dangerScore: this.countDangerStore(content),
    };
  }

  /**
   * Check if content contains any expected word from dictionary.
   *
   * @param {string} content - String to verify.
   */
  protected dictionaryOccurrences(content: string): number {
    let occurrences: number = 0;

    for (let word of this.dictionary) {
      if (0 <= content.indexOf(word)) {
        occurrences++;
      }
    }

    return occurrences;
  }

  /**
   * Check if content contains any expected word from suspicious words list.
   *
   * @param {string} content - String to verify.
   */
  protected countDangerStore(content: string): number {
    let score: number = 0;

    for (let word of Object.keys(this.wordList)) {
      if (0 <= content.indexOf(word)) {
        score += get(this.wordList, word, 0);
      }
    }

    return score;
  }

}